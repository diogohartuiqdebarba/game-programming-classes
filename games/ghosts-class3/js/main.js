const config = {
  type: Phaser.AUTO,
  width: 400,
  height: 300,
  zoom: 2,
  pixelArt: true,
  backgroundColor: "#222222",
  parent: "game-container",
  physics: {
    default: "arcade",
    arcade: {
      gravity: { y: 0 },
      debug: false
    }
  }
}
var game = new Phaser.Game(config)
game.scene.add('GameScene', GameScene, true, null)
