class GameScene extends Phaser.Scene {
  
  preload() {
    this.load.image("player", "./assets/player.png")
  }

  create() {
    this.player = this.physics.add.sprite(64, 180, "player")
    this.cursors = this.input.keyboard.createCursorKeys()
//    setTimeout(() => this.scene.start('EndGameScene'), 1000)
  }
  
  update() {
    if(this.cursors.left.isDown) {
      this.player.setVelocityX(-160)
      //player.anims.play('walkLeft', true)
    }else if(this.cursors.right.isDown) {
      this.player.setVelocityX(160)
      //player.anims.play('walkRight', true)
    }else{
      this.player.setVelocityX(0)
      //player.anims.play('idle')
    }

//    if(this.cursors.up.isDown && this.player.body.touching.down) {
//      this.player.setVelocityY(-330)
//    }
  }

}
