function FoodPool(scene) {
  this.group = scene.add.group({
    defaultKey: 'foodPoisonSpritesheet',
    maxSize: 100,
    createCallback: function(food) {
      food.setName('foodPoisonSpritesheet' + this.getLength())
//      console.log('Created', food.name)
    },
    removeCallback: function(food) {
//      console.log('Removed', food.name)
    }
  })
  
  this.activateFood = (food) => {
    const rd = Math.floor(Math.random() * 9)
    food.setActive(true)
        .setVisible(true)
        .setFrame(rd)
  }

  this.addFood = () => {
    const food = this.group.get(scene.game.canvas.width/2, scene.game.canvas.height/2)
    if(!food) return // None free
    this.activateFood(food)
    return food
  }
}
