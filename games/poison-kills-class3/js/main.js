const config = {
  type: Phaser.AUTO,
  width: 256 + 196,
  height: 256 + 196,
  zoom: 1,
  pixelArt: true,
  backgroundColor: "#a2a769",
  parent: "game-container",
  physics: {
    default: "arcade",
    arcade: {
      gravity: { y: 0 },
      debug: false
    }
  }
};
var game = new Phaser.Game(config);
game.scene.add('BootScene', BootScene, true, null);
game.scene.add('TutorialScene', TutorialScene, false, null);
game.scene.add('GameScene', GameScene, false, null);
game.scene.add('EndGameScene', EndGameScene, false, null);
game.scene.add('CreditsScene', CreditsScene, false, null);
