class CreditsScene extends Phaser.Scene {

  create() {
    const rect = new Phaser.Geom.Rectangle(0, 0, 256 + 196, 256 + 196)
    const graphics = this.add.graphics({fillStyle: {color: 0x211f17}})
    graphics.fillRectShape(rect)
    this.add.text(170, 16, 'CRÉDITOS', {fill: '#efefef'})
    this.add.text(50, 48, 'Assets:', {fill: '#e29494'})
    const authors = [
      {
        name: "@Kenney",
        assets: [
          {
            name: "OnScreen Controls Pack",
            link: "https://opengameart.org/content/onscreen-controls-8-styles"
          },
          {
            name: "UI Pack",
            link: "https://opengameart.org/content/ui-pack"
          }
        ]
      },
      {
        name: "@Ravenmore",
        assets: [
          {
            name: "Platformer Pickups Pack",
            link: "https://opengameart.org/content/platformer-pickups-pack"
          }
        ]
      },
      {
        name: "@Bevouliin",
        assets: [
          {
            name: "Poison Bottle Game Item",
            link: "https://opengameart.org/content/poison-bottle-game-item"
          }
        ]
      },
      {
        name: "@Diogo Hartuiq Debarba",
        assets: [
          {
            name: "All rest",
            link: "https://www.linkedin.com/in/diogohartuiqdebarba/"
          }
        ]
      }
    ]
    let posX = 60
    let posY = 48
    authors.forEach((author, i) => {
      posY += 20
      this.add.text(posX, posY, author.name, {fill: '#43578e'})
      author.assets.forEach((asset, j, arr) => {
        new TextButton(this, {
          x: posX, 
          y: posY + 20, 
          label: asset.name, 
          actionOnClick: () => window.open(asset.link, "_blank")
        })
        posY += 20
      })
    })
    this.add.text(230, 420, 'Game made in Phaser 3', {fill: '#be3842'})
    new TextButton(this, {
      x: 60, 
      y: 400, 
      label: 'Voltar', 
      actionOnClick: () => this.scene.start('TutorialScene')
    })
  }
}
