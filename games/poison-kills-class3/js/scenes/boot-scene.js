class BootScene extends Phaser.Scene {

  preload() {
    this.load.image("poisonBottleConcept", "./assets/sprites/poison-bottle/poison_bottle.png")
    this.load.spritesheet('spritesheetExample', './assets/ui-images/ui-pack-empty/Spritesheet/blueSheet.png', { 
      frameWidth: 190, 
      frameHeight: 48
    })
    this.load.atlasXML('buttonAtlas', 
                       './assets/ui-images/ui-pack-empty/Spritesheet/blueSheet.png',                         
                       './assets/ui-images/ui-pack-empty/Spritesheet/blueSheet.xml')
    this.load.spritesheet('foodPoisonSpritesheet', './assets/sprites/spritesheet-food-poison.png', { 
      frameWidth: 64, 
      frameHeight: 64
    })
  }

  create(data) {
    this.poisonBottleConcept = this.add.image(0, 0, 'poisonBottleConcept')
    this.poisonBottleConcept.setOrigin(0)
    this.poisonBottleConcept.displayWidth = this.game.canvas.width
    this.poisonBottleConcept.displayHeight = this.game.canvas.height
    const titleGame = this.add.text(104, 88, 'Poison Kills', {fill: '#ff0000'})
    titleGame.setFont("Roboto Condensed")
    titleGame.setFontSize(48)
    titleGame.setFontStyle("bold")
    const buttonStart = new ImageButton(this, {
      x: this.game.canvas.width/2, 
      y: this.game.canvas.height/2, 
      texture: 'buttonAtlas', 
      actionOnClick: () => {
        setTimeout(() => this.scene.start('TutorialScene'), 700);
      },
      outFrame: 'blue_button02.png',
      overFrame: 'blue_button00.png',
      upFrame: 'blue_button00.png', 
      downFrame: 'blue_button03.png'
    })
    const textStartGame = this.add.text(this.game.canvas.width/2 - 48, this.game.canvas.height/2 - 10, 'START GAME', {fill: '#ffffff'})
  }

}
