class GameScene extends Phaser.Scene {

  create() {
    this.add.text(32, 64, 'CONDIÇÃO:\n\nSE você consumir veneno ENTÃO fim de jogo.', {fill: '#ffffff'})
    this.add.text(16, this.game.canvas.height - 56, '[SPACE] = comer\n[DOWN(SETA PARA BAIXO)] = evitar o veneno', {fill: '#473e8d'})
    this.foodScore = 0
    this.foodScoreText = this.add.text(16, 16, 'SCORE: ' + this.foodScore, {fill: '#facb06'})
    this.foodPool = new FoodPool(this)
    this.spaceBar = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.SPACE)
    this.downArrow = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.DOWN)
    this.currentFood = this.foodPool.addFood()
    this.gameOver = false
  }
       
  update() {
    if(Phaser.Input.Keyboard.JustDown(this.downArrow) && !this.gameOver) {
      if(this.currentFood.frame.name === 8) {
        this.foodPool.group.killAndHide(this.currentFood)
        this.currentFood = this.foodPool.addFood()
      }
    }else if(Phaser.Input.Keyboard.JustDown(this.spaceBar) && !this.gameOver) {
      if(this.currentFood.frame.name !== 8) {
        this.foodPool.group.killAndHide(this.currentFood)
        this.foodScore++
        this.foodScoreText.setText('SCORE: ' + this.foodScore)
        this.currentFood = this.foodPool.addFood()
      }else{
        this.gameOver = true
        this.foodScoreText.setFill('#c91c0d')
        this.cameras.main.setBackgroundColor('#40e533')
        this.add.text(this.game.canvas.width/2 - 72, this.game.canvas.height/2 + 72, 'GAME OVER!', {fill: '#c91c0d', font: 'bold 25px Arial'})
        setTimeout(() => this.scene.start('EndGameScene'), 2000)
      }
    }
  }

}
