class TutorialScene extends Phaser.Scene {

  create() {
    const rect = new Phaser.Geom.Rectangle(0, 0, 256 + 196, 256 + 196);
    const graphics = this.add.graphics({ fillStyle: { color: 0x14380b } });
    graphics.fillRectShape(rect);
    this.add.text(180, 48, 'TUTORIAL', {fill: '#d0cc75'});
    this.add.text(22, 128, 'Pressione [SPACE] para consumir as comidas,\nmas cuidado com o veneno! Pressione [DOWN]\npara evitar o veneno.', {fill: '#efefef'});
    const startText = new TextButton(this, {
      x: 132, 
      y: 360, 
      label: 'Clique para iniciar o jogo.', 
      actionOnClick: () => this.scene.start('GameScene')
    })
  }
}
