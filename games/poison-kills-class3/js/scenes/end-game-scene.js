class EndGameScene extends Phaser.Scene {

  create(data) {
    const rect = new Phaser.Geom.Rectangle(0, 0, 256 + 196, 256 + 196)
    const graphics = this.add.graphics({ fillStyle: { color: 0x14380b } })
    graphics.fillRectShape(rect)
    this.add.text(170, 160, 'GAME OVER', {fill: '#efefef'})
    const startText = new TextButton(this, {
      x: 110, 
      y: 310, 
      label: 'Clique para jogar novamente.', 
      actionOnClick: () => this.scene.start('GameScene')
    })
    this.buttonStartTutorial = new TextButton(this, {
      x: 110, 
      y: 360, 
      label: 'Clique para ir para o tutorial.', 
      actionOnClick: () => this.scene.start('TutorialScene')
    })
    this.buttonCredits = new TextButton(this, {
      x: 110, 
      y: 410, 
      label: 'Clique para ver os créditos.', 
      actionOnClick: () => this.scene.start('CreditsScene')
    })
  }
}
