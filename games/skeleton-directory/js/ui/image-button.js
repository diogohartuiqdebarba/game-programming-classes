class ImageButton extends Phaser.GameObjects.Image {
  onInputOver = () => {}
  onInputOut = () => {}
  onInputUp = () => {}

  constructor(scene, options={}) {
    super(scene, options.x, options.y, options.texture, options.outFrame)
    scene.add.existing(this)
    .setInteractive()
    .on('pointerover', () => {
      this.onInputOver()
      if(options.overFrame)
        this.setFrame(options.overFrame)
    })
    .on('pointerdown', () => {
      options.actionOnClick()
      if(options.downFrame)
        this.setFrame(options.downFrame)
    })
    .on('pointerup', () => {
      this.onInputUp()
      if(options.upFrame)
        this.setFrame(options.upFrame)
    })
    .on('pointerout', () => {
      this.onInputOut()
      if(options.outFrame)
        this.setFrame(options.outFrame)
    })
  }
}
