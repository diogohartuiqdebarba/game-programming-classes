class TextButton extends Phaser.GameObjects.Text {
  onInputOver = () => {}
  onInputOut = () => {}
  onInputUp = () => {}

  constructor(scene, options={}) {
    super(scene, options.x, options.y, options.label, {fill: options.out || '#bea331'})
    scene.add.existing(this)
    .setInteractive()
    .on('pointerover', () => {
      this.onInputOver()
      this.setStyle({fill: options.fillOver || '#ff0014'})
    })
    .on('pointerdown', () => {
      options.actionOnClick()
      this.setStyle({fill: options.fillDown || '#249d2e'})
    })
    .on('pointerup', () => {
      this.onInputUp()
      this.setStyle({fill: options.fillUp || '#bea331'})
    })
    .on('pointerout', () => {
      this.onInputOut()
      this.setStyle({fill: options.fillOut || '#bea331'})
    })
  }
}
