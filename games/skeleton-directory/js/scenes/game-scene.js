class GameScene extends Phaser.Scene {

  create() {
    this.add.text(this.game.canvas.width/2 - 32, this.game.canvas.height/2 -32, 'MY GAME...', {fill: '#ffffff'})
    console.log("Game over at 1 seconds!");
    setTimeout(() => this.scene.start('EndGameScene'), 1000)
  }

}
