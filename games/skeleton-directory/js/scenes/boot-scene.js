class BootScene extends Phaser.Scene {

  preload() {
    preloadProgress.call(this);
    this.load.image("imageExample", "./assets/ui-images/ui-pack-empty/PNG/blue_circle.png");
    this.load.spritesheet('spritesheetExample', './assets/ui-images/ui-pack-empty/Spritesheet/blueSheet.png', { 
      frameWidth: 190, 
      frameHeight: 48
    });
    this.load.atlasXML('buttonAtlas', 
                    './assets/ui-images/ui-pack-empty/Spritesheet/blueSheet.png',                         
                    './assets/ui-images/ui-pack-empty/Spritesheet/blueSheet.xml');
  }

  create(data) {
    this.add.text(this.game.canvas.width/2 - 56, this.game.canvas.height/2 - 128, 'GAME EXAMPLE', {fill: '#d0cc75'});
    this.imageExample = this.add.image(this.game.canvas.width/2, this.game.canvas.height/2 - 156, 'imageExample');
    const buttonStart = new ImageButton(this, {
      x: this.game.canvas.width/2, 
      y: this.game.canvas.height/2, 
      texture: 'buttonAtlas', 
      actionOnClick: () => {
        setTimeout(() => this.scene.start('TutorialScene'), 700);
      },
      outFrame: 'blue_button02.png',
      overFrame: 'blue_button00.png',
      upFrame: 'blue_button00.png', 
      downFrame: 'blue_button03.png'
    })
    buttonStart.onInputOut = () => {
      console.log('Btn2: onInputOut')
    }
    const textStartGame = this.add.text(this.game.canvas.width/2 - 48, this.game.canvas.height/2 - 10, 'START GAME', {fill: '#ffffff'});
  }

}

function preloadProgress() {
    var progressBar = this.add.graphics();
    var progressBox = this.add.graphics();
    progressBox.fillStyle(0x222222, 0.8);
    progressBox.fillRect(240, 270, 320, 50);

    var width = this.cameras.main.width;
    var height = this.cameras.main.height;
    var loadingText = this.make.text({
        x: width / 2,
        y: height / 2 - 50,
        text: 'Loading...',
        style: {
            font: '20px monospace',
            fill: '#ffffff'
        }
    });
    loadingText.setOrigin(0.5, 0.5);

    var percentText = this.make.text({
        x: width / 2,
        y: height / 2 - 5,
        text: '0%',
        style: {
            font: '18px monospace',
            fill: '#ffffff'
        }
    });
    percentText.setOrigin(0.5, 0.5);

    var assetText = this.make.text({
        x: width / 2,
        y: height / 2 + 50,
        text: '',
        style: {
            font: '18px monospace',
            fill: '#ffffff'
        }
    });

    assetText.setOrigin(0.5, 0.5);

    this.load.on('progress', function (value) {
        percentText.setText(parseInt(value * 100) + '%');
        progressBar.clear();
        progressBar.fillStyle(0xffffff, 1);
        progressBar.fillRect(250, 280, 300 * value, 30);
    });

    this.load.on('fileprogress', function (file) {
        assetText.setText('Loading asset: ' + file.key);
    });

    this.load.on('complete', function () {
        progressBar.destroy();
        progressBox.destroy();
        loadingText.destroy();
        percentText.destroy();
        assetText.destroy();
    });
}
