function createMap() {
//  const map2 = [
//    [
//      [-1, -1, -1, -1, -1, -1, -1, -1],
//      [-1, -1, -1, -1, -1, -1, -1, -1],
//      [-1, -1, -1, -1, -1, -1, -1, -1],
//      [-1, -1, -1, -1, -1, -1, -1, -1],
//      [-1, -1, -1, -1, -1, -1, -1, -1],
//      [-1, -1, -1, -1, -1, -1, -1, -1],
//      [-1, -1, -1, -1, -1, -1, -1, -1],
//      [-1, -1, -1, -1, -1, -1, -1, -1]
//    ],
//    [
//      [3, 3, 3, 3, 0, 3, 3, 3],
//      [3, 3, 3, 3, 7, 3, 3, 3],
//      [4, 5, 5, -1, -1, -1, 5, 6],
//      [11, -1, -1, -1, -1, -1, -1, 13],
//      [11, -1, -1, -1, -1, -1, -1, 13],
//      [11, -1, -1, -1, -1, -1, -1, 13],
//      [11, -1, -1, -1, -1, -1, -1, 13],
//      [18, 19, 19, 19, 19, 19, 19, 20]
//    ],
//    [
//      [15, 15, 15, 15, 15, 15, 15, 15],
//      [15, 15, 15, 15, 15, 15, 15, 15],
//      [15, 15, 15, 15, 15, 15, 15, 15],
//      [15, 15, 15, 15, 15, 15, 15, 15],
//      [15, 15, 15, 15, 15, 15, 15, 15],
//      [15, 15, 15, 15, 15, 15, 15, 15],
//      [15, 15, 15, 15, 15, 15, 15, 15],
//      [15, 15, 15, 15, 15, 15, 15, 15]
//    ]
//
//  ]
//    const map = this.make.tilemap({data: map2, tileWidth: 32, tileHeight: 32});
//      scene.tilesetDoors = map.addTilesetImage("doors-spritesheet", "tiles");
//    const layer = map.createStaticLayer(0, tilesetDoors, 0, 0);
//    const layer1 = map.createStaticLayer(1, tilesetDoors, 0, 0);
//    const layer2 = map.createStaticLayer(2, tilesetDoors, 0, 0);
  this.map = this.make.tilemap({key: "map"});
  this.tilesetDoors = this.map.addTilesetImage("doors-spritesheet", "tiles");
  this.belowLayer = this.map.createStaticLayer("Below Player", this.tilesetDoors, 0, 0);
  this.worldLayer = this.map.createStaticLayer("World", this.tilesetDoors, 0, 0);
  createKey.call(this);
  createDoor.call(this);
  this.aboveLayer = this.map.createStaticLayer("Above Player", this.tilesetDoors, 0, 0);
  this.worldLayer.setCollisionByExclusion ([ 0 , -1 ]);
//  for(let i = 1; i < 22; i++)
//    if(i !== 2 && i !== 9 &&i !== 16)
//      this.worldLayer.setCollision(i); 
  this.aboveLayer.setDepth(10);
}
