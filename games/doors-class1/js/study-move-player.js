/* Code of Student */

function studyMovePlayer() {
  movePlayerLeftRightUpDown();
}

function movePlayerLeftRightUpDown() {
  const moves = [
    {
      direction: 'left',
      seconds: 1
    },
    {
      direction: 'right',
      seconds: 1
    },
    {
      direction: 'up',
      seconds: 1
    },
    {
      direction: 'down',
      seconds: 1
    }
  ];
  movePlayer(moves);
}

/* Code of Teacher */

async function movePlayer(moves) {
  const studyScene = game.scene.scenes[1];
  for(let i = 0; i < moves.length; i++) {
    studyScene.player.move(moves[i].direction);
    await sleep(moves[i].seconds * 1000);
    studyScene.player.move(); // stop move player
    await sleep(1000);
  }
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
