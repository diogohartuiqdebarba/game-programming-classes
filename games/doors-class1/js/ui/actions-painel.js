function createActionsPainel() {
  const rect = new Phaser.Geom.Rectangle(256, 0, 196, 256);
  const graphics = this.add.graphics({ fillStyle: { color: 0x403434 } });
  graphics.fillRectShape(rect);
  this.add.text(330, 12, 'Ações:', {fill: '#bea331'});
  this.buttonActionsMoveUp = createButton.call(this, 270, 32*2, 'Andar para frente', addAction('moveUp'));
  this.buttonActionsMoveDown = createButton.call(this, 270, 32*3, 'Andar para trás', addAction('moveDown'));
  this.buttonActionsMoveRight = createButton.call(this, 270, 32*4, 'Andar para direta', addAction('moveRight'));
  this.buttonActionsMoveLeft = createButton.call(this, 270, 32*5, 'Andar para esquerda', addAction('moveLeft'));
  this.buttonActionsGetKey = createButton.call(this, 270, 32*6, 'Pegar chave', addAction('getKey'));
  this.buttonActionsOpenDoor = createButton.call(this, 270, 32*7, 'Abrir porta', addAction('openDoor'));
}

function addAction(nameAction) {
  return function(button) {
    if(this.algorithmText.length < 100)
      this.algorithmText.push({nameAction: nameAction, buttonText: button.text});
    showAlgorithmText.call(this);
    if(this.algorithmText.length > (this.currentPageAlgorithmText + 1) * 5)
      nextPageAlgorithmText.call(this);
  }
}
