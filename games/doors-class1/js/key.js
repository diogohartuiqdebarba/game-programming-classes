function createKey() {
  this.key = this.physics.add.sprite(190, 170, "tilesSpritesheet", 14).setScale(0.5);
  this.key.setCollideWorldBounds(true);
  this.key.getKey = function() {
    this.player.inventary.push({name: 'key', amount: 1});
    this.keyItem = this.add.sprite(306, 290, "tilesSpritesheet", 14);
    this.key.destroy();
  };
}
