function createDoor() {
  this.door = this.physics.add.sprite(128 + 16, 32 + 16, "tilesSpritesheet", 7);
  this.door.col = this.physics.add.sprite(128 + 16, 0 + 16, "tilesSpritesheet", 0);
  this.door.col.setCollideWorldBounds(true);
  this.door.col.body.immovable = true;
  this.door.col.body.moves = false;
  this.door.openDoor = function(fn) {
    this.door.col.setCollideWorldBounds(false);
    this.door.setFrame(8);
    this.door.col.setFrame(1);
    fn.call(this);
  }
}
