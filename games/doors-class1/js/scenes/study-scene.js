class StudyScene extends Phaser.Scene {

  create() {
    createMap.call(this);
    if(this.game.config.physics.arcade.debug)
       debugLayerCollides.call(this, this.worldLayer);
    createPlayer.call(this);
    this.physics.add.collider(this.player, this.worldLayer);
    this.physics.add.collider(this.player, this.door.col);
    this.physics.add.overlap(this.player, this.key, null, null, this);
    this.physics.add.overlap(this.player, this.door, null, null, this);
    this.cursors = this.input.keyboard.createCursorKeys();
    studyMovePlayer();
  }

}
