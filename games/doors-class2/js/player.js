function createPlayer() {
  this.player = this.physics.add.sprite(64, 180, "char", "left-walk").setScale(1.4);
  this.player.inventary = [];
  this.player.body.setSize(16, 32);
  this.player.body.setOffset(8, 0);
  this.anims.create({
    key: "down-walk",
    frames: this.anims.generateFrameNames("char", {start: 0, end: 2}),
    frameRate: 4,
    repeat: -1
  });
  this.anims.create({
    key: "left-walk",
    frames: this.anims.generateFrameNames("char", {start: 3, end: 5}),
    frameRate: 4,
    repeat: -1
  });
  this.anims.create({
    key: "right-walk",
    frames: this.anims.generateFrameNames("char", {start: 6, end: 8}),
    frameRate: 10,
    repeat: -1
  });
  this.anims.create({
    key: "up-walk",
    frames: this.anims.generateFrameNames("char", {start: 9, end: 11}),
    frameRate: 10,
    repeat: -1
  });
  this.player.setCollideWorldBounds(true);
  this.player.move = function(direction) {
    if(direction === 'left') {
      this.player.body.setVelocityX(-100);
      this.player.anims.play("left-walk", true);
    }else if(direction === 'right') {
      this.player.body.setVelocityX(100);
      this.player.anims.play("right-walk", true);
    }else if(direction === 'down') {
      this.player.body.setVelocityY(-100);
      this.player.anims.play("up-walk", true);
    }else if(direction === 'up') {
      this.player.body.setVelocityY(100);
      this.player.anims.play("down-walk", true);
    }else{
      this.player.body.setVelocityX(0);
      this.player.body.setVelocityY(0);
      this.player.anims.stop();
    }
  }.bind(this);
  
}
