const config = {
  type: Phaser.AUTO,
  width: 256,
  height: 256,
  backgroundColor: "#222222",
  parent: "game-container",
  physics: {
    default: "arcade",
    arcade: {
      gravity: { y: 0 },
      debug: true
    }
  }
};
const game = new Phaser.Game(config);
game.scene.add('BootScene', BootScene, true, {study: true});
game.scene.add('StudyScene', StudyScene, false, null);
