function createActionsPainel() {
  const rect = new Phaser.Geom.Rectangle(256, 0, 196, 256);
  const graphics = this.add.graphics({ fillStyle: { color: 0x403434 } });
  graphics.fillRectShape(rect);
  this.add.text(330, 12, 'Ações:', {fill: '#bea331'});
  this.buttonActionsMoveUp = createButton.call(this, 270, 32*2, 'Manipular Variáveis \ne Constantes', () => this.scene.start('VarScene'));
}

function showVarPainel(button) {
  createVarPainel.call(this);
}
