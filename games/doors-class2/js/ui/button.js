function createButton(x, y, label, fn) {
  const button = this.add.text(x, y, label, {fill: '#bea331'})
  .setInteractive({useHandCursor: true})
  .on('pointerdown', () => {
    enterButtonActiveState(button);
    fn.call(this, button);
  })
  .on('pointerover', () => enterButtonHoverState(button))
  .on('pointerout', () => enterButtonRestState(button))
  .on('pointerup', () => enterButtonHoverState(button));
  return button;
}

function enterButtonHoverState(button) {
  button.setStyle({fill: '#ff0014'});
}

function enterButtonRestState(button) {
  button.setStyle({fill: '#bea331'});
}

function enterButtonActiveState(button) {
  button.setStyle({fill: '#249d2e'});
}
