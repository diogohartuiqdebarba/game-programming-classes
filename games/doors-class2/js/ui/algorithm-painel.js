function createAlgorithmPainel() {
  const rect = new Phaser.Geom.Rectangle(0, 256, 256 + 196, 196);
  const graphics = this.add.graphics({ fillStyle: { color: 0x211f17 } });
  graphics.fillRectShape(rect);
  this.successAlgorithm = false;
  this.game.algorithmText = this.game.algorithmText || [];
  this.currentPageAlgorithmText = this.currentPageAlgorithmText || 0;
  this.add.text(64, 270, 'Seu algoritmo:', {fill: '#bea331'});
  this.textAlgorithm = this.add.text(8, 270 + 32, '', {fill: '#bea331'});
  this.buttonNextPage = createButton.call(this, 40, 270 + 160, 'Anterior', backPageAlgorithmText);
  this.buttonNextPage = createButton.call(this, 40 + 96, 270 + 160, 'Próxima', nextPageAlgorithmText);
  const rectInventaryBorder = new Phaser.Geom.Rectangle(270, 262, 74, 56);
  this.add.graphics({fillStyle: {color: 0x774e16}}).fillRectShape(rectInventaryBorder);
  const rectInventary = new Phaser.Geom.Rectangle(270 + 5, 262 + 5, 74 - 10, 56 - 10);
  this.add.graphics({fillStyle: {color: 0x28330d}}).fillRectShape(rectInventary);
  const rectClean = new Phaser.Geom.Rectangle(270, 270 + 56, 174, 56);
  this.add.graphics({fillStyle: {color: 0x7c7a7a}}).fillRectShape(rectClean);
  this.buttonCleanAlgorithm = createButton.call(this, 288, 270 + 70, 'Limpar algoritmo!', cleanAlgorithm);
  const rectExecute = new Phaser.Geom.Rectangle(270, 270 + 56 + 64, 174, 56);
  this.add.graphics({fillStyle: {color: 0x1e4027}}).fillRectShape(rectExecute);
  this.buttonExecuteAlgorithm = createButton.call(this, 280, 270 + 136, 'Executar algoritmo!', executeAlgorithm);
  showAlgorithmText.call(this);
  if(this.game.algorithmText.length > (this.currentPageAlgorithmText + 1) * 5)
    nextPageAlgorithmText.call(this);
}

function showAlgorithmText() {
  let stg = '';
  for(let i = this.currentPageAlgorithmText * 6; i < this.game.algorithmText.length && i < ((this.currentPageAlgorithmText + 1) * 6); i++) {
    stg += i + ' - ' + this.game.algorithmText[i].buttonText + '\n';
  }
  this.textAlgorithm.setText(stg);
}

function backPageAlgorithmText(button) {
  if(this.currentPageAlgorithmText > 0)
    this.currentPageAlgorithmText -= 1;
  showAlgorithmText.call(this);
}

function nextPageAlgorithmText(button) {
  if(this.currentPageAlgorithmText < (this.game.algorithmText.length/6) - 1)
    this.currentPageAlgorithmText += 1;
  showAlgorithmText.call(this);
}

function cleanAlgorithm(button) {
  if(!this.game.runAlgorithm) {
    this.game.algorithmText = [];
    this.game.algorithmVars = [];
    this.game.algorithmConst = [];
    this.currentPageAlgorithmText = 0;
    showAlgorithmText.call(this);
  }
}

const actions = {
  moveUp: function() {
    this.player.move('up');
  },
  moveDown: function() {
    this.player.move('down');
  },
  moveRight: function() {
    this.player.move('right');
  },
  moveLeft: function() {
    this.player.move('left');
  },
  moveStop: function() {
    this.player.move();
  },
  getKey: function() {
    if(checkOverlap(this.player, this.key))
      this.key.getKey.call(this);
  },
  openDoor: function() {
    const haveKey = this.player.inventary.find((el) => el.name === 'key');
    if(checkOverlap(this.player, this.door) && haveKey)
      this.door.openDoor.call(this, () => this.successAlgorithm = true);
  }
};

function checkOverlap(spriteA, spriteB) {
  const boundsA = spriteA.getBounds();
  const boundsB = spriteB.getBounds();
  return Phaser.Geom.Intersects.RectangleToRectangle(boundsA, boundsB);
}

async function executeAlgorithm(button) {
  this.game.runAlgorithm = true;
  for(let i = 0; i < this.game.algorithmText.length; i++) {
    for(let j = 0; j < this.game.algorithmText[i].repeat; j++) {
      actions[this.game.algorithmText[i].nameAction].call(this);
      await sleep(200);
      actions['moveStop'].call(this);
      await sleep(1000);
    }
  }
  this.game.algorithmText = [];
  this.game.algorithmVars = [];
  this.game.algorithmConst = [];
  showAlgorithmText.call(this);
  this.currentPageAlgorithmText = 0;
  endGame.call(this);
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function endGame() {
  this.scene.start('EndGameScene', {success: this.successAlgorithm});
}
