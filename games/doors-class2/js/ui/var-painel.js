function createVarPainel() {
  this.varName = '';
  this.varAction = '';
  this.varValue = 0;
  this.selectedVarOrConst = null;
  this.selectedVarName = null;
  this.selectedVarValue = null;
  this.varOrConst = '';
  this.game.algorithmVars = this.game.algorithmVars || [];
  this.game.algorithmConst = this.game.algorithmConst || [];
  const rect = new Phaser.Geom.Rectangle(0, 0, 256 + 196, 256 + 196);
  const graphics = this.add.graphics({ fillStyle: { color: 0x403434 } });
  graphics.fillRectShape(rect);
  this.buttonActionsMoveUp = createButton.call(this, 380, 16, 'Voltar', () => this.scene.start('GameScene'));
  this.buttonActionsMoveUp = createButton.call(this, 48, 32 , 'var', setVarOrConst('var'));
  this.buttonActionsMoveUp = createButton.call(this, 48 + 48, 32, 'const', setVarOrConst('const'));
  createVarNamePainel.call(this);
  createVarValuePainel.call(this);
  const rectAttribution = new Phaser.Geom.Rectangle(130, 64+32*9+16, 120, 56);
  this.add.graphics({fillStyle: {color: 0x1e4027}}).fillRectShape(rectAttribution);
  this.buttonAttribution = createButton.call(this, 130 + 48, 64+32*9+32, 'Ok', addVarAction(false));
}

function createVarNamePainel() {
  this.add.text(32, 64, 'Nome:', {fill: '#bea331'});
  this.buttonActionsMoveUp = createButton.call(this, 48, 64+32, 'andarParaFrente', setNameAction('moveUp'));
  this.buttonActionsMoveDown = createButton.call(this, 48, 64+32*2, 'andarParaTrás', setNameAction('moveDown'));
  this.buttonActionsMoveRight = createButton.call(this, 48, 64+32*3, 'andarParaDireta', setNameAction('moveRight'));
  this.buttonActionsMoveLeft = createButton.call(this, 48, 64+32*4, 'andarParaEsquerda', setNameAction('moveLeft'));
  this.buttonActionsGetKey = createButton.call(this, 48, 64+32*5, 'pegarChave', setNameAction('getKey'));
  this.buttonActionsOpenDoor = createButton.call(this, 48, 64+32*6, 'abrirPorta', setNameAction('openDoor'));
}

function createVarValuePainel() {
  this.add.text(32, 64+32*7, 'Valor:', {fill: '#bea331'});
  this.buttonActionsMoveUp = createButton.call(this, 48, 64+32*8, '1', setValue(1));
  this.buttonActionsMoveUp = createButton.call(this, 48+32, 64+32*8, '2', setValue(2));
  this.buttonActionsMoveUp = createButton.call(this, 48+32*2, 64+32*8, '3', setValue(3));
  this.buttonActionsMoveUp = createButton.call(this, 48+32*3, 64+32*8, '4', setValue(4));
  this.buttonActionsMoveUp = createButton.call(this, 48+32*4, 64+32*8, '5', setValue(5));
  this.buttonActionsMoveUp = createButton.call(this, 48+32*5, 64+32*8, '6', setValue(6));
  this.buttonActionsMoveUp = createButton.call(this, 48+32*6, 64+32*8, '7', setValue(7));
  this.buttonActionsMoveUp = createButton.call(this, 48+32*7, 64+32*8, '8', setValue(8));
  this.buttonActionsMoveUp = createButton.call(this, 48+32*8, 64+32*8, '9', setValue(9));
  this.buttonActionsMoveUp = createButton.call(this, 48+32*9, 64+32*8, '10', setValue(10));
}

function setVarOrConst(varOrConst) {
  return function(button) {
    this.varOrConst = varOrConst;
    if(this.selectedVarOrConst)
      this.selectedVarOrConst.destroy();
    selectVarOrConst.call(this, button.x, button.y, 44, 22);
  }
}

function setNameAction(actionName) {
  return function(button) {
    this.varName = button.text;
    this.varAction = actionName;
    if(this.selectedVarName)
      this.selectedVarName.destroy();
    selectName.call(this, button.x, button.y, 160, 20);
  }
}

function setValue(value) {
  return function(button) {
    this.varValue = value;
    if(this.selectedVarValue)
      this.selectedVarValue.destroy();
    selectValue.call(this, button.x, button.y, 22, 22);
  }
}

function selectVarOrConst(x, y, withRect, heightRect) {  
  this.selectedVarOrConst = this.add.graphics();
  const color = 0xd31010;
  const thickness = 2;
  const alpha = 1;
  this.selectedVarOrConst.lineStyle(thickness, color, alpha);
  this.selectedVarOrConst.strokeRect(x, y, withRect, heightRect);
}

function selectName(x, y, withRect, heightRect) {  
  this.selectedVarName = this.add.graphics();
  const color = 0xd31010;
  const thickness = 2;
  const alpha = 1;
  this.selectedVarName.lineStyle(thickness, color, alpha);
  this.selectedVarName.strokeRect(x, y, withRect, heightRect);
}

function selectValue(x, y, withRect, heightRect) {
  this.selectedVarValue = this.add.graphics();
  const color = 0xd31010;
  const thickness = 2;
  const alpha = 1;
  this.selectedVarValue.lineStyle(thickness, color, alpha);
  this.selectedVarValue.strokeRect(x, y, withRect, heightRect);
}

function addVarAction(isDeclared) {
  return function(button) {
    if(this.game.algorithmText.length < 100) {
      if(checkExpections.call(this)) {
        const textVar = this.varOrConst + (this.varOrConst ? ' ' : '') + this.varName + '=' + this.varValue + ';';
        this.game.algorithmText.push({nameAction: this.varAction, buttonText: textVar, repeat: this.varValue});
        this.scene.start('GameScene');
      }
    }    
  }
}

function checkExpections() {
  const existVar = this.game.algorithmVars.find((el) => el === this.varName);
  const existConst = this.game.algorithmConst.find((el) => el === this.varName);
  if(!this.varName) {
    showWarning.call(this, "Você ainda não selecionou o \nnome da variável ou constante!");
    return false;
  }else if(!this.varValue) {
    showWarning.call(this, "Você ainda não selecionou o \nvalor da variável ou constante!");
    return false; 
  }else if(this.varOrConst === 'var' && existVar) {
    showWarning.call(this, "Você está tentando declarar \numa variável que já foi declarada!");
    return false;
  }else if(this.varOrConst === 'const' && existConst) {
    showWarning.call(this, "Você está tentando declarar \numa constante que já foi declarada!");
    return false;
  }else if(this.varOrConst === 'var' && existConst && !existVar) {
    showWarning.call(this, "Você está tentando declarar uma \nvariável com o nome de uma \nconstante já declarada!");
    return false;
  }else if(this.varOrConst === 'const' && existVar && !existConst) {
    showWarning.call(this, "Você está tentando declarar uma \nconstante com o nome de uma \nvariável já declarada!");
    return false;
  }else if(this.varOrConst === 'var' && !existVar) {
    this.game.algorithmVars.push(this.varName);
    return true;
  }else if(this.varOrConst === 'const' && !existConst) {
    this.game.algorithmConst.push(this.varName);
    return true;
  }else if(!this.varOrConst && !existVar && !existConst) {
    showWarning.call(this, "Você está tentando atribuir \numa variável/constante que \nNÃO foi declarada!");
    return false;
  }else if(!this.varOrConst && existConst) {
    showWarning.call(this, "Você está tentando alterar \no valor de uma constante!");
    return false;
  }else if(!this.varOrConst && existVar) {
    return true;
  }else if(!this.varOrConst && existConst) {
    return true;
  }
}

function showWarning(text) {
  const rectWarning = new Phaser.Geom.Rectangle(48, 96, 320, 120);
  const warningPainel = this.add.graphics({fillStyle: {color: 0x93906b}}).fillRectShape(rectWarning);
  const warningTitle = this.add.text(48 + 120, 96 + 8, 'AVISO', {fill: '#ff0d03'});
  const warningText = this.add.text(48 + 8, 96 + 32, text, {fill: '#d11919'});
  setTimeout(() => {
    warningPainel.destroy();
    warningTitle.destroy();
    warningText.destroy();
  }, 3000);
}
