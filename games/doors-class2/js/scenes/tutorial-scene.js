class TutorialScene extends Phaser.Scene {

  create() {
    const rect = new Phaser.Geom.Rectangle(0, 0, 256 + 196, 256 + 196);
    const graphics = this.add.graphics({ fillStyle: { color: 0x0f0e0b } });
    graphics.fillRectShape(rect);
    this.add.text(180, 48, 'TUTORIAL', {fill: '#d0cc75'});
    this.add.text(40, 96, 'O Problema:', {fill: '#efefef'});
    this.add.text(40, 128, 'Você precisa sair do local onde está, \nhá uma porta, talvez você precise de uma \nchave para escapar. Crie um algoritmo \npara tentar sair de onde está. Desta vez você \nutilizará variáveis e constantes para criar \nseu algoritmo.', {fill: '#efefef'});
    this.buttonStartGame = createButton.call(this, 132, 360, 'Clique para iniciar o jogo.', () => this.scene.start('GameScene'));
  }
}
