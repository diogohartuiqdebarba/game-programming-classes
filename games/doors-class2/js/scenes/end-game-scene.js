class EndGameScene extends Phaser.Scene {

  create(data) {
    const rect = new Phaser.Geom.Rectangle(0, 0, 256 + 196, 256 + 196);
    const graphics = this.add.graphics({ fillStyle: { color: 0x211f17 } });
    graphics.fillRectShape(rect);
    this.add.text(170, 160, 'FIM DE JOGO', {fill: '#efefef'});
    if(data.success)
      this.add.text(92, 230, 'Seu algoritmo resolveu o problema.', {fill: '#22b222'});
    else   
      this.add.text(68, 230, 'Seu algoritmo NÃO resolveu o problema!', {fill: '#d32424'});
    this.buttonStartGameAgain = createButton.call(this, 110, 310, 'Clique para jogar novamente.', () => {
      this.game.runAlgorithm = false;
      this.scene.start('GameScene');
    });
    this.buttonStartTutorial = createButton.call(this, 110, 360, 'Clique para ir para o tutorial.', () => this.scene.start('TutorialScene'));
    this.buttonCredits = createButton.call(this, 116, 410, 'Clique para ver os créditos.', () => this.scene.start('CreditsScene'));
  }

}
