class CreditsScene extends Phaser.Scene {

  create() {
    const rect = new Phaser.Geom.Rectangle(0, 0, 256 + 196, 256 + 196);
    const graphics = this.add.graphics({ fillStyle: { color: 0x211f17 } });
    graphics.fillRectShape(rect);
    this.add.text(170, 16, 'CRÉDITOS', {fill: '#efefef'});
    this.add.text(70, 32, 'Assets:', {fill: '#e29494'});
    this.add.text(60, 32 + 32, 'Character:', {fill: '#43578e'});
    createButton.call(this, 60, 32 + 64, '@pixelcitybros', () => {
      window.open("https://opengameart.org/content/chadmandoos-random-2d-rpg-characters-animations-larger-characters", "_blank");
    });
    this.add.text(60,  32 + 96, '2D game Newton Adventure Pack:', {fill: '#43578e'});
    createButton.call(this, 60, 32 + 128, '@devnewton', () => {
      window.open("https://opengameart.org/content/newton-adventure-sprites-and-tiles", "_blank");
    });
    this.add.text(60, 32 + 160, 'Dungeon Tileset:', {fill: '#43578e'});
    createButton.call(this, 60, 32 + 192, '@HorusKDI', () => {
      window.open("https://opengameart.org/content/dungeon-tileset-4", "_blank");
    });
    this.add.text(60, 32 + 224, 'TileMap made in Tiled:', {fill: '#43578e'});
    createButton.call(this, 60, 32 + 256, 'Tiled', () => {
      window.open("https://www.mapeditor.org/", "_blank");
    });
    this.add.text(60, 32 + 292, 'All rest:', {fill: '#43578e'});
    createButton.call(this, 60, 32 + 324, '@Diogo Hartuiq Debarba', () => {
      window.open("https://www.linkedin.com/in/diogohartuiqdebarba/", "_blank");
    });
    this.add.text(60, 32 + 356 , 'Game made in Phaser 3', {fill: '#be3842'});
    this.buttonStartTutorial = createButton.call(this, 360, 420, 'Voltar', () => this.scene.start('TutorialScene'));
  }

}
