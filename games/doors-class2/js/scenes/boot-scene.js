class BootScene extends Phaser.Scene {

  preload() {
    preloadProgress.call(this);
    this.load.image("doors", "./assets/doors-loading2.png");
    this.load.image("tiles", "./assets/doors-spritesheet.png");
    this.load.tilemapTiledJSON("map", "./assets/doors.json");
    this.load.spritesheet('char', './assets/chpepper1squirePNG.png', { 
      frameWidth: 32, 
      frameHeight: 32
    });
    this.load.spritesheet('tilesSpritesheet', './assets/doors-spritesheet.png', { 
      frameWidth: 32, 
      frameHeight: 32
    });
  }

  create(data) {
    if(data && data.study)
      this.doors = this.add.image(128, 140, 'doors');
    else
      this.doors = this.add.image(230, 200, 'doors');
    setTimeout(() => {
      if(data && data.study)
        this.scene.start('StudyScene')
      else
        this.scene.start('TutorialScene')
    }, 3000);
  }

}

function preloadProgress() {
    var progressBar = this.add.graphics();
    var progressBox = this.add.graphics();
    progressBox.fillStyle(0x222222, 0.8);
    progressBox.fillRect(240, 270, 320, 50);

    var width = this.cameras.main.width;
    var height = this.cameras.main.height;
    var loadingText = this.make.text({
        x: width / 2,
        y: height / 2 - 50,
        text: 'Loading...',
        style: {
            font: '20px monospace',
            fill: '#ffffff'
        }
    });
    loadingText.setOrigin(0.5, 0.5);

    var percentText = this.make.text({
        x: width / 2,
        y: height / 2 - 5,
        text: '0%',
        style: {
            font: '18px monospace',
            fill: '#ffffff'
        }
    });
    percentText.setOrigin(0.5, 0.5);

    var assetText = this.make.text({
        x: width / 2,
        y: height / 2 + 50,
        text: '',
        style: {
            font: '18px monospace',
            fill: '#ffffff'
        }
    });

    assetText.setOrigin(0.5, 0.5);

    this.load.on('progress', function (value) {
        percentText.setText(parseInt(value * 100) + '%');
        progressBar.clear();
        progressBar.fillStyle(0xffffff, 1);
        progressBar.fillRect(250, 280, 300 * value, 30);
    });

    this.load.on('fileprogress', function (file) {
        assetText.setText('Loading asset: ' + file.key);
    });

    this.load.on('complete', function () {
        progressBar.destroy();
        progressBox.destroy();
        loadingText.destroy();
        percentText.destroy();
        assetText.destroy();
    });
}
