# Aula 2 -  Variáveis e Constantes

## O que preciso para estudar essa aula?

Para iniciar nesta segunda aula, você precisa ter respondidas as perguntas abaixo, que são referentes a [Aula 1](pt-br/aula1-algoritmos-e-abstracoes.md):

* O que é um algoritmo?

* O que é uma abstração?

* Como construir um algoritmo para resolver um problema?

* Como abstrair partes de um algoritmo?

## Introdução

### O que são variáveis e constantes? 

Na programação lidamos bastante com dados, esses dados precisam estar armazenados em algum lugar na memória RAM do
computador para podermos manipulá-los, assim, guardamos esses dados numa variável ou constante.

As variáveis e constantes podem ser nomeadas, para assim serem utilizadas dentro de nosso código, no Javacript criamos
váriaveis utilizando `var` ou `let`, mais para frente veremos a diferença entre as duas formas, e para criar constantes utilizamos
`const`.

Como assim? Veja um exemplo onde estou criando (declarando) uma variável de nome "x":

```javascript
var x;
```

Podemos atribuir um valor para uma váriavel ao declará-la ou após sua declaração. No exemplo abaixo estou declarando uma 
variável de nome "x" e depois atribuindo uma valor 1 para ela:

```javascript
var x;
x = 1;
```
Perceba que não utilizei mais `var` na segunda linha onde faço a atribuição do valor 1 a variável de nome "x", isso porque
`var` é utilizado para declarar variáveis, e depois que elas já foram declaradas, não é mais necessário utilizar de novo o `var`, 
pois a variável já existe em nosso código e pode ser chamada apenas com o nome que utilizamos na declaração da mesma, no caso "x".

Também podemos atribuir um valor a uma váriavel em sua declaração:

```javascript
var x = 1;
```

Economizando uma linha de código, pois declaramos a variável de nome "x" já atribuindo o valor 1 para ela.

Repare no `;`, utilizamos o `;` no Javascript para finalizar instruções, nos exemplos de código acima para finalizar nossas declarações e
atribuições.

### Qual é a diferença entre variável e constante?

Uma váriavel é um dado armazenado no computador, chamamos de "variável", pois o valor contido nesse espaço de memória pode variar com o
tempo.

Uma constante é um dado armazenado no computador, chamamos de "constante", pois o valor contido nessse espaço de memória é fixo, ou seja,
diferente das variáveis, **NÃO** pode mudar com o tempo.

Resumindo, uma váriavel pode receber um valor que pode ser alterada no decorrer do nosso jogo, e uma constante pode receber um valor
que é fixo e não pode ser alterado no decorrer do nosso jogo.

### Para que serve uma variável ou constante?

Utilizaremos variáveis e constantes para armazenar os valores que necessitamos ao criar nosso jogos, você já deve ter
jogado algum jogo onde você tinha "5 vidas", então lá em sua tela você veria "5 vidas restantes" no canto superior esquerdo, guardamos esse valor `5` na memória RAM do computador através de uma variável, então poderiamos criar uma váriavel de nome "vidas" assim:

```javascript
var vidas = 5; 
```
No caso das vidas, como podemos perder vidas, ou até ganhar vidas, utilizamos uma variável, pois o valor da variável "x" pode mudar no decorrer do jogo.

Também poderiamos criar uma constante chamada "textoQueAcompanhaAsVidas" assim:

```javascript
const textoQueAcompanhaAsVidas = "vidas restantes"; 
```
Criamos uma constante e não uma variável para armazenar o valor `"vidas restantes"`, pois não queremos alterar esse texto durante o jogo,
então a constante de nome "textoQueAcompanhaAsVidas" terá o valor `"vidas restantes"` do inicio ao fim do jogo, enquanto a variável de nome
"vidas" terá de inicio o valor `5` que pode ser decrementado ou incrementado no decorrer do jogo.

### Alguns dos tipos de dados primitivos do Javascript

Um tipo de dado é utilizado para definir os valores que um dado pode assumir, mas olhar para o exemplo da váriavel e constante que criamos
anteriormente, a váriavel declarada com o nome "vidas" tem um valor inicialmente atribuido que é `5`, o valor`5` é um número, então o seu
tipo de dado é o "Número" (no inglês "Number").

A constante declarada com o nome "textoQueAcompanhaAsVidas" tem um valor inicialmente atribuido que é "vidas restantes", o valor "vidas restantes" é uma cadeia de caracteres, pois está dentro de aspas, então o seu tipo de dado é "Cadeia de caracteres" (no inglês "String").

Há ainda outros tipos de dados primitivos no Javascript, e veremos mais dois deles, antes saiba que existem tipos de dados primitivos, e tipos
de dados não primitivos, os que estamos vendo nesta aula são apenas os primitivos, mais para frente veremos os tipos de dados não primitivos.

Os outros dois tipos de dados que veremos são "booleano" e "nulo":

O tipo de dado booleano (no inglês "Boolean") só podem receber dois valores, ou "true" (verdadeiro) ou "false" (falso).

O tipo de dado nulo (no inglês "null") pode receber apenas um valor, que é o "null" (nulo).

Vejamos alguns exemplos dos tipos de dados:

```javascript
var score = 3;
var speed = 1.5;
const gravity = 9.2;
const textButton = "Start!";
var isDead = false;
var isJumping = true;
var leftHand = null;
var rightHand = "ball";
```
Sim, está tudo em inglês, você terá que aprender um pouco dessa linguagem para lidar com a programação, as documentações de códigos
geralmente estão todas em inglês, e é uma boa pratica escrever seu código em inglês. Irei ajudar deixando os significados abaixo das
palavras acima:

<details><summary>Aprendendo Inglês</summary>
  <ul>
    <li><b>score</b> -> pontos, lembre-se da contagem de seus pontos em um jogo</li>
    <li><b>speed</b> -> velocidade, lembre-se da velocidade que o seu personagem do jogo anda ou corre</li>
    <li><b>gravity</b> -> gavidade, como a gravidade dos planetas</li>
    <li><b>textButton</b> -> texto do botão</li>
    <li><b>Start</b> -> começar/iniciar</li>
    <li><b>isDead</b> -> está morto?</li>
    <li><b>isJumping</b> -> está pulando?</li>
    <li><b>leftHand</b> -> mão esquerda</li>
    <li><b>rightHand</b> -> mão direita</li>
    <li><b>ball</b> -> bola</li>
  </ul>
</details>

## Estudando Jogando

[filename](../games/doors-class2/index.html ':include :type=iframe width=100% height=480px scrolling=no')

## Exercícios para agora

1. O que é uma variável?

2. O que é uma constante?

3. O que significa "atribuir" um valor a uma variável ou constante?

4. O que é um tipo de dado?

5. Quais foram os 4 tipos de dados que aprendemos acima?

## Programando

Vamos mexer no código do jogo "Doors" do tópico "Estudando Jogando" novamente. Na função "movePlayerLeftRightUpDown" temos a
constante "moves" que é um array de objetos, ainda não vamos entrar nos array e objetos, que são tipos não primitivos. Iremos
brincar um pouco com o código que temos.

No caso da função "movePlayerLeftRightUpDown", estamos repetindo o número "1" para todos os movimentos, então podemos criar uma
constante para substituir esses números soltos. Veja:

```javascript
function movePlayerLeftRightUpDown() {
  const oneSecond = 1;
  const moves = [
    {
      direction: 'left',
      seconds: oneSecond
    },
    {
      direction: 'right',
      seconds: oneSecond
    },
    {
      direction: 'up',
      seconds: oneSecond
    },
    {
      direction: 'down',
      seconds: oneSecond
    }
  ];
  movePlayer(moves);
}
```
Eu criei uma constante de nome "oneSecond" e substitui com os números soltos de minha função, rodando o jogo, veremos que o
resultado é o mesmo de antes. Isso porque não alteramos nada, só organizamos melhor o código e nomeamos o número solto `1`
que estava se repetindo em nosso código.

Poderiamos criar uma váriavel ao invés de uma constante, assim:

```javascript
function movePlayerLeftRightUpDown() {
  var oneSecond = 1;
  const moves = [
    {
      direction: 'left',
      seconds: oneSecond
    },
    {
      direction: 'right',
      seconds: oneSecond
    },
    {
      direction: 'up',
      seconds: oneSecond
    },
    {
      direction: 'down',
      seconds: oneSecond
    }
  ];
  movePlayer(moves);
}
```

O resultado seria o mesmo, mas como não temos a intenção de mudar o valor da variável "oneSecond" durante a execução do
jogo, uma constante é uma escolha melhor para esse caso.

Como exercício você pode fazer o mesmo com o resto de suas funções em que hajam números repetidos ou strings repetidas.

Irei passar dois problemas para fixar o que foi aprendido sobre variáveis e constantes:

**O Problema 1:**

>Abaixo "oneSecond" e "twoSeconds" podem ser variáveis ou constantes? qual o mais adequado e porque?

```javascript
function movePlayerLeftRightUpDown() {
  ? oneSecond = 1;
  ? twoSeconds = 2;
  const moves = [
    {
      direction: 'left',
      seconds: oneSecond
    },
    {
      direction: 'right',
      seconds: oneSecond
    },
    {
      direction: 'up',
      seconds: twoSeconds
    },
    {
      direction: 'down',
      seconds: twoSeconds
    }
  ];
  movePlayer(moves);
}
```
**O Problema 2:**

>Abaixo "Seconds" deve ser uma variável ou constante, e porque?

```javascript
function movePlayerLeftRightUpDown() {
  ? seconds = 1;
  seconds = 3;
  const moves = [
    {
      direction: 'left',
      seconds: seconds
    },
    {
      direction: 'right',
      seconds: seconds
    },
    {
      direction: 'up',
      seconds: seconds
    },
    {
      direction: 'down',
      seconds: seconds
    }
  ];
  movePlayer(moves);
}
```

## O que aprendi hoje?

O que é e para que serve uma variável?

O que é e para que serve uma constante?

O que são tipos de dados?

Saber que existem tipos de dados primitivos e não primitivos.

Alguns dos tipos de dados primitivos do Javascript.

Como utilizar váriaveis e constantes em um algoritmo.

## Exercícios para casa

1. Para os tipos de dados abaixo:

    - booleano (Em inglês é "boolean")
    - número (Em inglês é "number")
    - cadeia de caracteres (Em inglês é "string")
    - nulo (Em inglês é "null")
    
   Qual é o tipo de dado dos valores abaixo:
    - Exemplo: false = booleano/boolean
    
   *Lembre-se que em inglês "true" quer dizer "verdadeiro" em português, e "false"
   é "falso".*
  
    - true = 
    - 1 =
    - 3.5 =
    - 0 =
    - 0.0 =
    - 1.1 =
    - 6000897 =
    - 0.455555 =
    - "tudo bem?" =
    - null =
    - 'ok!' =
    - "1244456ac" =
    - false =

2. Uma variável de nome "x" recebe o valor 1, ela pode ter seu valor alterado enquanto o loop jogo está sendo executado?

3. Uma constante de nome "x" recebe o valor 1, ela pode ter seu valor alterado enquanto o loop jogo está sendo executado?

4. Leia o conteúdo abaixo:
<details><summary>O que são Literais:</summary>
<p>
  
  Hoje você aprendeu o que é tipo de dado e também o que é um valor, também aprendeu como atribuir um valor a 
  uma variável ou constante, agora você saberia dizer o que é um "literal"? Neste capítulo vimos diversos literais,
  e utilizamos eles para representar os valores, então um literal é um valor fixado, ou seja, não variável, que é
  utilizado para representar valores. O que? um valor utilizado para representar um valor? sim, na programação
  as vezes caimos em umas questões assim, mas não se assute, vamos olhar para os exemplos do exercício 1 e você irá ver que
  é fácil, pois você já estava utilizando os "literais" sem saber quem eles são.

  Exemplos de literais booleanos (boolean):

  ```javascript
  false
  true
  ```

  Exemplos de literais de números (Number):

  ```javascript
  1         // literal de número inteiro (integer)
  3.5       // literal de número de ponto flutuante (float)
  0         // literal de número inteiro (integer)
  0.0       // literal de número de ponto flutuante (float)
  1.1       // literal de número de ponto flutuante (float)
  6000897   // literal de número inteiro (integer)
  0.455555  // literal de número de ponto flutuante (float)
  ```

  Exemplos de literal nulo (null):

  ```javascript
  null
  ```

  Exemplos de literais de cadeia de caracteres (string):

  Literais de string são zero ou mais caracteres dispostos em aspas simples ('') ou aspas duplas ("").

  ```javascript
  "tudo bem?"
  'ok!'
  "1244456ac"
  ```

  O que você precisa entender é que usamos os literais para representar valores, então dado uma variável de nome "minhaVariavel"
  atribuindo de início um valor 1, como abaixo:

  ```javascript
  var minhaVariavel = 1;
  ```
  O `1` é um literal que utilizamos para representar o valor 1. E como é uma variável, podemos mudar o valor dela para, por exemplo, 5:

  ```javascript
  minhaVariavel = 5;
  ```
  Agora `5` é um literal que utilizamos para representar o valor 5. 

  Vamos para mais um exemplo, mas desta vez utilizando uma constante. Dado uma constante de nome "minhaConstante" atribuindo de início
  um valor true, como abaixo:

  ```javascript
  const minhaConstante = true;
  ```
  Agora `true` é um literal que utilizamos para representar o valor true. Como se trata de uma constante, não podemos mudar o valor dela,
  se tentássemos mudar o valor dela para false, por exemplo, receberiamos um erro no compilador/interpretador do Javascript dizendo que não é possivel alterar o valor de uma constante.

  Perceba que utilizei acima `var` e `const` e também `;`, `var` no Javascript é utilizada para criar variáveis, `const` para criar constantes,
  e `;` para finalizar instruções. 

  Você pode buscar saber mais sobre [valores, variáveis e literais aqui](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Guide/Values,_variables,_and_literals) ou [aqui no Wikipédia - Literal (programação de computadores)](https://pt.wikipedia.org/wiki/Literal_(programa%C3%A7%C3%A3o_de_computadores)).

</p>
</details>
