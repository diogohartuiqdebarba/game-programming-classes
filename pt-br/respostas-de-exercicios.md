# Respostas de Exercícios

## Aula 1 - Algoritmos e Abstrações

### Programando

**Problema 1:**

```javascript
/* Code of Student */

function studyMovePlayer() {
  movePlayerLeftRightUpDown();
  movePlayerRightLeft();
  movePlayerRightRightLeftLeft();
}

function movePlayerRightRightLeftLeft() {
  const moves = [
    {
      direction: 'left',
      seconds: 1
    },
    {
      direction: 'right',
      seconds: 1
    },
    {
      direction: 'up',
      seconds: 1
    },
    {
      direction: 'down',
      seconds: 1
    }
  ];
  movePlayer(moves);
}
```

**Problema 2:**

```javascript
/* Code of Student */

function studyMovePlayer() {
  movePlayerLeftRightUpDown();
  movePlayerRightLeft();
  movePlayerRightRightLeftLeft();
  movePlayerRight3seconds();
}

function movePlayerRight3seconds() {
  const moves = [
    {
      direction: 'right',
      seconds: 3
    }
  ];
  movePlayer(moves);
}
```

## Aula 2 - Variáveis e Constantes

### Programando

**Problema 1:**

Ambas, "oneSecond" ou "twoSeconds" podem ser variáveis ou constantes, porém o mais adequado seria criá-las como
constantes, já que não temos intenção de alterar seus valores em tempo de execução.

```javascript
const oneSecond = 1;
const twoSeconds = 2;
```

**Problema 2:**

A "seconds" deve ser uma variável, porque estamos alterando seu valor em tempo de execução, se fosse uma constante receberiamos
um erro por atribuir `3` a "seconds" em tempo de execução.

```javascript
var seconds = 1;
```