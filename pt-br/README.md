# Aulas de Programação de Jogos

## Introdução

Olá, está pronto para aprender a criar jogos?

Se sim, me acompanhe. 

Aqui estudaremos programação de jogos utilizando a linguagem [Javascript(ECMAScript)](https://pt.wikipedia.org/wiki/JavaScript), se ainda não a conhece, de uma lida no link anterior, 
saiba que você está utilizando Javascript agora mesmo, ao ler essa página. O Javascript é a linguagem
mais utilizada para criar páginas interativas. Entretanto, o Javascript não se limita a criação de
páginas web, ele também pode ser utilizado para criação de jogos, e ao meu ver, apesar de ser uma
liguagem um tanto "confusa", é uma linguagem agradável para estudar criação de jogos, pois podemos 
ver o resultado rapidamente em qualquer navegador.

## Didática

A didática das aulas é voltada para um público mais jovem, que apesar de não ter conhecimentos
avançados de computação, lidam com ela desde novo. Algumas aulas irão utilizar mini-jogos para passar
o conteúdo desejado. No decorrer das aulas, o estudante irá modificar/acrescentar novas fases/cenários/funcionalidades
a esses jogos, todos utilizando uma API bem simples, pois o foco não será a programação em si, mas a criação
de jogos, e tentarei focar ainda mais na diversão ao criá-los. Após assistir as aulas mais avançadas, 
o estudante deverá ser capaz de criar jogos "do zero".

## Como funcionarão as aulas

As aulas serão divididas em partes (isso não é regra, pode ser que algumas aulas fujam um pouco desse método):

**1 - O que preciso para estudar essa aula?**

Aqui o estudante irá pesquisar o que for necessário e mostrar os exercícios para casa feitos.

**2 - Introdução**

Aqui será apresentado o conteúdo da aula.

**3 - Estudando Jogando**

Aqui o estudante irá jogar um jogo para fixar o conteúdo da aula.

**4 - Exercícios para agora**

Aqui o estudante fará exercícios sobre o conteúdo da aula.

**5 - Programando**

Aqui o estudante irá codificar, algumas aulas ele irá modificar o jogo de estudo, outras irá criar.

**6 - O que aprendi hoje?**

Você deve ter aprendido tudo que estiver aqui, pois na aula seguinte irá precisar do que foi dado nesta aula.

**7 - Exercícios para casa**

Exercícios para fazer em casa, com o objetivo de fixar o conteúdo estudado até aqui.

## O que as aulas irão abordar

As aulas abordarão a computação em geral, o desenvolvimento de jogos utilizando um [Motor de jogo](https://pt.wikipedia.org/wiki/Motor_de_jogo) 
ou em inglês chamado de "Game Engine". Utilizaremos o motor de jogo em Javascript chamado [Phaser](http://www.phaser.io/).
O estudante irá aprender a manipular esse Motor de Jogo para criar seus jogos. Também tentarei passar o que aprendi no
desenvolvimento de jogos, passando um pouco sobre arte/música/roteiro. Será ensinado ao estudante a aprender a aprender,
o que é algo necessário para se tornar um bom programador, e também saber fazer as perguntas certas, para obter as
respostas para resolver seus problemas, afinal, programação é isso, resolver problemas, tudo sempre dará errado, e você
como programador irá consertar os erros, que na maioria das vezes você mesmo os causou. O estudante aprenderá detalhes 
da criação de jogos, e a diminuir suas grandes e impossiveis ideias para jogos que funcionem e sejam divertidos, não que
não será incentivado a exploração de ideias, isso é essencial, porém será ensinado algo que demorei aprender: evitar 
frustações por meio ruminação de ideias e código coberto com testes.
