# Aula 3 - Condicionais e Operadores - (Game Ghosts: Movimentação do personagem)

## O que preciso para estudar essa aula?

Para iniciar nesta terceira aula, você precisa ter estudado as aulas 1 e 2, onde na primeira aula estudamos algoritimos
e abstrações, e na segunda aula, variáveis e constantes. Você também precisará de algum programa para criação de sprite 
(sprites são as imagens do jogo, por exemplo um personagem, ou uma árvore; em jogos 2D, usamos sprites),
se você é iniciante eu recomendo o programa gratuito [Piskel](https://www.piskelapp.com), caso você tenha mais conhecimento 
na criação de arte, recomendo o [Aseprite](https://www.aseprite.org/), que é disponivel gratuitamente para
quem utiliza Linux, você só precisa saber compilá-lo ([detalhes de como compilar](https://github.com/aseprite/aseprite/blob/master/INSTALL.md)). Esses são programas para criar [pixel art](https://pt.wikipedia.org/wiki/Pixel_art).

## Introdução

### O que é pixel art?

É uma arte digital onde as images (quais chamamos de sprites) são criadas utilizando como elemento básico os
[pixels](https://pt.wikipedia.org/wiki/Pixel) da tela, aqueles quadradinhos pequenininhos que formam a tela, pixel
seria a menor unidade que você pode atribuir uma cor, o conjunto dos pixels formam a tela. Então pintamos os pixels
para criar um sprite que utilizaremos em nossos jogos.

Exemplos de pixel art:

![Pig Chef by One Man Army](../images/condicionais-operadores-img1.png)

*Pig Chef feita por [One Man Army](https://opengameart.org/users/one-man-army)*

![Cutted Tree by Pixquare](../images/condicionais-operadores-img2.png)

*Cutted Tree feita por [Pixquare](https://opengameart.org/users/pixquare)*

![Gold Icon by Cough-E](../images/condicionais-operadores-img3.png)

*Gold Icon feita por [Cough-E](https://opengameart.org/users/cough-e)*

Perceba que são imagens bem pequenas, quantos mais pixels você utilizar mais dificil (não necessáriamente, depende da qualidade
do que você está produzindo) será criá-los. Iremos trabalhar com artes bem simples de 32x32 pixels apenas.

### Criando um personagem no Piskel

Irei utilizar o programa [Piskel](https://www.piskelapp.com) para criar meu personagem, você não precisa criar o personagem
como o que estou criando, seja criativo e invente algum outro, para iniciar vamos criar um personagem estático, apenas uma imagem,
como nesta aula estaremos iniciando a construção do primeiro jogo do zero, que será um [jogo de plataforma](https://pt.wikipedia.org/wiki/Jogo_eletr%C3%B4nico_de_plataforma), ele poderá se movimentar para direita ou esquerda e também pular, mas
um passo de cada vez, primeiro vamos tentar mover nosso sprite estático na tela utilizando de condicionais, qual é o foco desse capítulo.

Antes de começar a construir seu jogo, é interessante você criar um esboço do mesmo no papel ou em qualquer lugar que queira, tentando
organizar suas ideias e conceitos que irão dar a personalidade de seu jogo. Veja o esboço que criei do jogo que iremos construir:

![Esboço de jogo](../images/condicionais-operadores-img4.jpeg)

Temos um personagem que iremos criar o sprite dele, e um cenário, e várias latas de lixo ou barris que ele irá pular, enquanto fantasmas 
correm atrás dele, o jogo se passará inicialmente em um cenário noturno, para acentuar a ideia dos fantasmas. Esse esboço é só nosso
ponto de partida, iremos desenvolver as ideias ao ir criando o jogo, mas é interessante você pensar no maximo de coisas que puder
antes de criar o jogo, claro não fique tempo demais e se esqueça de criar o jogo, é importante começar a fazer as coisas também, se não
nunca saimos do lugar, e também muito do que pensamos temos que mudar, ou melhorar, ou até deixar de fora de nosso jogo, pois vamos descobrindo o que fica mais divertido e funciona melhor no desenvolvendo do jogo.

O personagem que criei foi esse abaixo, sim bem mal feito, você consegue fazer um melhor que esse, perceba como é pequeno em 32x32 pixels, no jogo poderemos aumentar ele para ficar maior e mais fácil de viasualizar.

![Esboço de jogo](../images/condicionais-operadores-img5.png)

### O que são condicionais?

Uma condicional é como uma pergunta, imagine que você tenha no bolso 2 moedas, se você tiver duas moedas de um real então pode comprar um
garrafinha d'agua, senão não pode comprar uma garrafinha d'agua. 

Então uma condicional verificar se algo é verdadeiro ou falso, está lembrado do tipo de dado Booleano que estudamos na aula 2? aqui
aplicaremos o que estudamos naquela aula nas condicionais, pois uma condicional verifica se algo é verdadeiro ou falso, se for verdadeiro faça isso, se for falso não faça ou faça outra coisa.

Perceba que utilizo "se", "então", "senão", na programação cada linguagem tem suas proprias palavras chaves para essas palavras,
no Javascript "se" é "if", e "if" nada mais é que traduzindo para o portugês "se". Já o "então", no Javascript não temos uma palavra chave que representar "então", pois utilizamos blocos para colocar o que acontece se uma condição é dada como verdadeira ou falsa, para o "senão" no
Javascript utilizamos a palavra chave "else". 

### if

Vamos com um exemplo, primeiro em português:

*Se estiver noite, então estou dormindo*

Agora no Javascript:

```javascript
var statePlayer = "awake";
var isNight = true;

if(isNight === true) {
  statePlayer = "sleeping";
}
``` 

<details><summary>Aprendendo Inglês</summary>
  <ul>
    <li><b>StatePlayer</b> -> estado do jogador</li>
    <li><b>isNight</b> -> é noite?</li>
    <li><b>awake</b> -> acordado</li>
    <li><b>sleeping</b> -> dormindo</li>
  </ul>
</details>

Veja acima tenho uma variável do tipo String chamada de `statePlayer` onde armazeno o estado do jogador que inicialmente
está "awake", e depois tenho outra variável de nome `isNight` do tipo Booleano que está sendo `true` (verdadeiro). Então
abaixo da declaração + atribuição das variáveis temos a condicional se `isNight` for verdadeiro, utilizamos o operador de igualdade
`===`, assim verificamos: "`isNight` é verdadeiro?", sendo, então é executado o código que está dentro do bloco `{}`, onde mudamos o valor
de `statePlayer` para "sleeping".

Você também poderia escrever o código de uma maneira mais curta, pois o tipo Booleano só pode ser true ou false, podemos utilizá-lo assim:

```javascript
var statePlayer = "awake";
var isNight = true;

// Maneiras de dizer "Se estiver noite, então estou dormindo":
if(isNight === true) {
  statePlayer = "sleeping";
}

if(isNight) {
  statePlayer = "sleeping";
}

// Maneiras de dizer "Se não estiver noite, então estou dormindo":
if(isNight === false) {
  statePlayer = "sleeping";
}

if(!isNight) {
  statePlayer = "sleeping";
}
``` 
O operador lógico NOT no Javascript é representado `!`, ele serve para negar uma expressão, no caso acima, estamos negando `isNight`. Iremos 
estudar os operadores lógicos e condicionais mais a frente.

### if ... else

Agora vamos ver o "senão" em Javascript, primeiro em português:

*Se estiver noite, então estou dormindo, senão estou acordado*

Agora no Javascript:

```javascript
var statePlayer = "awake";
var isNight = true;

if(isNight) {
  statePlayer = "sleeping";
}else{
  statePlayer = "awake";
}
```

Veja acima utilizamos o `else` para representar o `senão`, então se `isNight` for verdadeiro, atribua o valor que representa o estado de
dormindo a variável `statePlayer`, senão atribua a variável `statePlayer` o valor que representa o estado de acordado. Como atribuir valor
a uma variável é algo comum no Javascript, podemos escrever isso de uma maneira mais curta:

```javascript
var statePlayer = "awake";
var isNight = true;

statePlayer = isNight ? "sleeping" : "awake";
```

Onde temos nossa condição `isNight` é verdadeiro? se sim, então atribua "sleeping" a `statePlayer` (o `?` é como se fosse o então), 
senão atribua "awake" a `statePlayer` (e o `:` é como se fosse o senão). Isso se chama de [operador ternário](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Operators/Operador_Condicional). Cuidado que na forma curta, é obrigatório o uso do `else`, então você não poderia escrever algo como:

```javascript
var statePlayer = "awake";
var isNight = true;

statePlayer = isNight ? "sleeping";
```

Isso causaria um erro, é obrigatório ter o valor para caso a condição não seja verdadeira após o `:`.

### if ... else if

E se precisamos de mais um estado para nosso jogador? já temos "acordado", "dormindo", poderiamos ter outro como "faminto" (no inglês "hungry'), temos o `else if` para nos auxiliar, veja, primeiro em português:

```
Se estiver noite
  então estou dormindo
se estiver na hora do almoço 
  então estou faminto
senão 
  estou acordado
```

Agora no Javascript:

```javascript
var statePlayer = "awake";
var isNight = true;
var isLunchTime = true;

if(isNight) {
  statePlayer = "sleeping";
}else if(isLunchTime) {
  statePlayer = "hungry";
}else{
  statePlayer = "awake";
}
```

Veja que acima criei outra variável chamada `isLunchTime` (em português: "está na hora do almoço?") atribuindo `true`, perceba 
que agora se executarmos o código a variável `statePlayer` irá receber o valor ainda "sleeping" porque `isNight` é `true`, o `else if`
trabalha assim, ele verifica primeiro o `if`, se for `true`, executa o código do bloco `if` e não verifica as próximas condicionais, 
se for `false` vai para a próxima condição (se existir), no caso acima temos mais uma para verificar, então vai para o `else if`, se for `true`, executa o código do bloco `else if`, se for `false` vai para o próximo, o ultimo sempre é `else`, que é genérico, se nenhuma das condições for sastisfeita, então executa o que estiver no bloco de código do `else`.

Atente-se que agora não podemos utilizar a forma curta, pois temos aqui mais de uma condicional para verificar.

### Operadores de igualdade e desigualdade

Quando queremos verificar se um valor é igual ou diferente utilizamos os operadores da tabela abaixo:


Operador  | Descrição                             | Exemplo
--------- | ------------------------------------- | -----------
==        | é igual                               | a == b
===       | é igual e do mesmo tipo               | a === b
!=        | é diferente                           | a != b
!==       | é diferente e não é do mesmo tipo     | a !== b

Perceba que no Javascript podemos verificar tanto se um valor é igual/desigual ou igual/desigual e do mesmo/não mesmo tipo, o que isso quer dizer:

```javascript
var a = 1;
var b = "1";

if(a == b) {
  console.log(true);
}else {
  console.log(false);
}
```

Ao executar o código acima, no console veremos: `true`, porque 1 e "1" é a mesma coisa para o Javascript, quando não há a verificação
de tipos. 

Agora com verificação de tipos:

```javascript
var a = 1;
var b = "1";

if(a === b) {
  console.log(true);
}else {
  console.log(false);
}
```

Ao executar o código acima, no console veremos: `false`, pois a variável `a` é do tipo **Number** e a variável `b` é do tipo **String**, 
portanto não são do mesmo tipo.

Isso também segue a mesma lógica para os operadores de desigualdade `!=` que não verifica tipos, e `!==` que verifica tipos.

### Operadores relacionais

Operador  | Descrição                             | Exemplo
--------- | ------------------------------------- | -----------
<         | menor que                             | a < b
>         | maior que                             | a > b
<=        | menor ou igual                        | a <= b
>=        | maior ou igual                        | a >= b

Veja exemplos de uso dos operadores relacionais:

```javascript
var a = 1;
var b = 2;

if(a > b) {
  console.log(true);
}else {
  console.log(false);
}
```

Ao executar o código acima, no console veremos: `false`, pois `a` é menor do que `b`.

```javascript
var a = 1;
var b = 2;

if(a < b) {
  console.log(true);
}else {
  console.log(false);
}
```

Ao executar o código acima, no console veremos: `true`, pois `a` é menor do que `b`.

```javascript
var a = 1;
var b = 1;

if(a < b) {
  console.log(true);
}else {
  console.log(false);
}
```

Ao executar o código acima, no console veremos: `false`, pois `a` é igual a `b`, e estamos verificando apenas se `a` é
menor que `b`.

```javascript
var a = 1;
var b = 1;

if(a <= b) {
  console.log(true);
}else {
  console.log(false);
}
```

Ao executar o código acima, no console veremos: `true`, pois `a` é igual a `b`, e estamos verificando se `a` é
menor ou igual a `b`.

Isso segue a mesma lógica para verificar se é "maior que" e "maior e igual".

### Operadores lógicos

Lembra que falei do operador lógico NOT que é o operador de negação, no Javascript representado por `!`. Ainda veremos mais dois
que são muito utilizados para construir condicionais, que são os operadores AND (no português, "e") e OR (no português, "ou").

O operador lógico AND é representado no Javascript por `&&`, e serve para adicionar mais expressões a serem verificadas, por exemplo:

```javascript
var statePlayer = "awake";
var isNight = true;
var isLunchTime = true;

if(statePlayer === "awake" && isLunchTime) {
  statePlayer = "hungry";
}

```

Se queremos apenas deixar o player com fome se ele estiver acordado, podemos fazer como acima, adicionar duas expressões para serem
verificadas numa mesma condicional, evitando de fazer algo como:

```javascript
var statePlayer = "awake";
var isNight = true;
var isLunchTime = true;

if(statePlayer === "awake") {
  if(isLunchTime) {
    statePlayer = "hungry";
  }
}

```

O que vai deixando o código mais confuso, imagine se aninhamos um monte de `if` como que ficaria a leitura do código? você pode
adicionar várias expressões, é só ir acrescentando o operado lógico seguido da expressão desejada, exemplo:

```javascript
var statePlayer = "awake";
var isNight = true;
var isLunchTime = true;
var emptyStomach = false;

if(statePlayer === "awake" && isLunchTime && emptyStomach) {
  statePlayer = "hungry";
}

```

Acima estamos verificando se o player está acordado, se é hora do almoço e se seu estômago está vazio numa mesma condicional, se atente
que as expressões são verificadas da esquerda para a direita, então primeiro é verificado se a variável `statePlayer` tem o valor "awake", 
depois se `isLunchTime` é verdadeiro, e somente depois é verificado se `emptyStomach` é verdadeiro, se por exemplo `statePlayer` tiver um
valor diferente de "awake", já não entra no bloco `if`, nem mesmo é verificada as outras expressões, pois o operador lógico AND, é obrigatório
que todas as expressões sejam verdadeiras.

Já o operador lógico OR, que no Javascript é representado por `||`, também pode ser usado nas condicionais, mas ao invés de precisar de que todas as expressões sejam sastisfeitas, com o operador OR, apenas uma delas precisa ser sastifeita para que entre no bloco, veja um exemplo:

```javascript
var statePlayer = "awake";
var isNight = true;
var isLunchTime = true;
var emptyStomach = false;

if(statePlayer === "awake" || isLunchTime) {
  statePlayer = "hungry";
}

```

Acima verifico se `statePlayer` tem o valor "awake" ou se `isLunchTime` é verdadeiro, como `statePlayer` tem o valor "awake", qual
é atribuido acima na declaração da variável, não é verificada a outra expressão , pois já entra no bloco de código do `if`. Agora veja
outro exemplo:

```javascript
var statePlayer = "awake";
var isNight = true;
var isLunchTime = true;
var emptyStomach = false;

if(statePlayer === "sleeping" || isLunchTime) {
  statePlayer = "hungry";
}

```

Agora verificamos se `statePlayer` tem o valor "sleeping", como isso é falso, então é verificada a próxima exepressão, como `isLunchTime`
é verdadeiro, também é executado o código do bloco `if`, pois uma das duas expressões foi sastifeita. Como o operador AND, você pode incluir
mais expressões com `||`, exemplo:

```javascript
var statePlayer = "awake";
var isNight = true;
var isLunchTime = true;
var emptyStomach = false;

if(statePlayer === "sleeping" || isLunchTime || !emptyStomach) {
  statePlayer = "hungry";
}

```

Acima verificamos se `statePlayer` tem o valor "sleeping" ou se `isLunchTime` é verdadeiro, ou se `emptyStomach` é falso, veja que usei
o operador NOT `!` para negar a expressão `emptyStomach`. Só uma dessas expressões precisa ser verdadeira para que entre no bloco `if`, 
lembrando que quando uma expressão for verdadeira, as outras não serão verificadas, e já entrará no bloco `if`.


Então perceba a diferença, o operador NOT `!` serve para negar uma expressão, o operador AND `&&` serve para adicionar expressões
a serem verificadas, sendo necessário que todas elas sejam verdadeiras, enquanto o operador OR `||` serve verificar uma ou outra expressão
é verdadeira, sendo obrigatório que apenas uma das expressões seja verdadeira.

Saiba que ainda existe uma outra condicional no Javascript chamada [switch](https://developer.mozilla.org/pt-BR/docs/Web/JavaScript/Reference/Statements/switch), mas não entrarei em detalhes nela, você pode buscar mais no link.

## Estudando Jogando

[filename](../games/poison-kills-class3/index.html ':include :type=iframe width=100% height=480px scrolling=no')

## Exercícios para agora

1. O que é uma condicional?

2. Dê um exemplo da utilização de uma condicional em português?

3. Como é representado as condicionais no Javascript?

4. Como utilizamos o **if**, **else** e **else if**?

5. Dê um exemplo de um operador de igualdade/desigualdade do Javascript?

6. Dê um exemplo de um operador relacional do Javascript?

7. Dê um exemplo de um operador lógico do Javascript?

8. Dê um exemplo de utilização de uma condicional utilizando algum dos operadores do Javascript.

## Programando

Iremos fazer o personagem que criamos com o Piskel andar pela tela para direita e esquerda, quando pressionarmos as
teclas para fazê-lo mover. Para isso utilizaremos as condicionais que aprendemos acima.

O Phaser 3 trabalha com o conceito de cenas, então os jogos são geralmente construídos com várias cenas, uma cena do menu, 
uma cena do jogo, uma cena game over, uma cena de créditos, ou uma cena para cada fase do jogo, tudo depende de como você 
está construindo seu jogo. Iremos criar apenas uma cena onde tudo acontece inicialmente, assim quando sentirmos a necessidade
de criar outras cenas para deixar tudo mais organizado, faremos.

Primeiro vamos olhar como é a arvore de diretórios do nosso jogo:

![Árvore de diretórios inicial do jogo Ghosts](../images/condicionais-operadores-img6.png)

Perceba na imagem acima como é um diretório básico e organizado para armazenar os códigos e assets (imagens, sons, fontes...) do
seu jogo. Gosto de manter os assets dentro de um diretório "assets/" reservado para eles. Como também de manter toda a lógica escrita em
Javascript em um diretório chamado "js/", e também de manter as cenas em seu próprio diretório, o arquivo "main.js" é o arquivo que irá
criar o game utilizando o Phaser, o responsavel por criar a tela Canvas do HTML5 onde veremos o nosso jogo "rodar". O arquivo "index.html"
fica no diretório raiz, ele é necessário pois estamos criando jogos para web, que rodaram no navegador, então será o arquivo que o navegador
irá olhar quando acessamos o diretório do nosso jogo daquela maneira utilizando o Web Server no Chromium. Vamos começar olhando para o arquivo "index.html":

```html
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Ghosts</title>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <style>
    #game-container {
      min-width: 100vw;
      min-height: 100vh;
      display: flex;
      align-items: center;
      justify-content: center;
      margin-left: .5em;
    }
  </style>
</head>
<body>
  <div id="game-container"></div>
  <script src="../phaser.js"></script>
  <script src="./js/scenes/game-scene.js"></script>
  <script src="./js/main.js"></script>
</body>
</html>
```

Veja no código acima que temos a **div** onde estará a tela de nosso jogo que o Phaser irá criar para nós de acordo com as configurações
do arquivo "main.js". E também carregamos 3 scripts, que são os arquivos Javascript que temos em nossa árvore de diretórios do jogo. 
Também estou configurando para que a tela do jogo fique no centro de nossa tela do navegador, isso faço no código CSS inline que está
dentro da tag "style" acima.

Vejamos o arquivo "main.js":


```javascript
const config = {
  type: Phaser.AUTO,
  width: 400,
  height: 300,
  zoom: 2,
  pixelArt: true,
  backgroundColor: "#222222",
  parent: "game-container",
  physics: {
    default: "arcade",
    arcade: {
      gravity: { y: 0 },
      debug: false
    }
  }
}
var game = new Phaser.Game(config)
game.scene.add('GameScene', GameScene, true, null)
```

Veja acima que temos um objeto `config` que possui várias configurações para nosso jogo Phaser: A largura e altura da tela em "width" e "height"; por ser pixel art e estamos trabalhando com images 32x32, que são muito pequenas, eu preferi configurar um "zoom" e avisar ao Phaser que estamos trabalhando com pixel art para nossos sprites não ficarem borrados. Configurei também a cor de fundo da tela do jogo em "backgroundColor". O "parent" é a **div** pai onde estará a tela Canvas do jogo, lembra no arquivo "index.html" da **div** de id "game-container". Configuro também a física do jogo em "physics", mantive a gravidade em 0 porque primeiro vamos apenas mover nosso personagem na tela, depois colocaremos um chão para que ele não fique caindo infinitamente, ai sim podemos configurar a gravidade, qual será útil para os  pulos do personagem.

E agora nossa unica cena de jogo chamada "GameScene" no arquivo "./scene/game-scene.js":


```javascript
class GameScene extends Phaser.Scene {

  preload() {
    this.load.image("player", "./assets/player.png")
  }

  create() {
    this.player = this.physics.add.sprite(64, 180, "player")
    this.cursors = this.input.keyboard.createCursorKeys()
  }
  
  update() {
    if(this.cursors.left.isDown) {
      this.player.setVelocityX(-160)
    }else if(this.cursors.right.isDown) {
      this.player.setVelocityX(160)
    }else{
      this.player.setVelocityX(0)
    }
  }

}
``` 

É aqui onde nosso jogo acontece, pense num jogo assim, temos um loop, onde fica girando e girando e girando, esse é o loop do nosso game Phaser que criamos no arquivo "main.js", dentro desse loop são chamadas as funções de atualização (no inglês: "update") de todas as cenas, onde acontece a lógica do jogo, como por exemplo, faça algo se um botão for clicado, ou tire vida do personagem se ele for atacado, ou mude o texto de score da tela se o jogador conseguiu mais pontos.

No código acima na primeira linha criamos uma classe chamada "GameScene", eu ainda não ensinei o que são classes, e não há problema nisso, mais para frente você verá, só saiba que classes servem para criar objetos. Essa classe herda de `Phaser.Scene` que é uma classe padrão do Phaser, assim herdamos todas as caracteristicas de uma cena de jogo do Phaser. Mas não ligue muito para isso por agora, vamos continuar estudando o código.

Depois vemos uma função `preload()`, ela será chamada antes de tudo, e apenas uma vez, sendo assim não será chamada no loop do jogo, será chamada antes de iniciar o loop do jogo, então é um bom lugar para carregamos os assets, no nosso caso apenas a imagem do personagem que criamos no Piskel. Então eu chamo a função `load.image()` da cena, o `this` é uma forma de se referir a própria cena "GameScene", essa função recebe dois argumentos, o primeiro o nome que daremos para o asset, e o segundo o caminho onde ele se encontra, que é dentro do driretório "assets", onde tenho a imagem "player.png" que criei e exportei no Piskel.

Em seguida temos a função `create()` que será chamada depois da função `preload` apenas uma vez assim como a função `preload()`, nela criamos um sprite na cena "GameScene" utilizando o `this` para fazer referência a "GameScene" e chamando a função `physics.add.sprite()`, e  configuramos a posição x e y onde o Phaser irá desenhar nosso personagem na tela, e o terceiro argumento é o nome da imagem, qual passamos "player" que foi o nome de que demos para nosso asset na função `load.image()`, com isso já temos nosso personagem desenhado na tela.

Contudo, queremos fazer ele se mover da direita para esquerda, então precisamos configurar três condições, uma para quando pressionarmos a tecla "arrow right" que é a seta para direita do teclado, e outra para quando pressionarmos a tecla "arrow left" que é a seta para esquerda do teclado, e ainda outra para quando não estivermos pressionando tecla nenhuma, para podemos deixar o player parado. 

Então primeiro configuramos um `this.cursors` na função `create()`, útil para podermos pegar as teclas **Up**, **Down**, **Left** e **Right**,  também a **Space Bar** e **shift**, que são teclas muito utilizadas e por isso o Phaser fornece essa função para elas, para outras teclas temos que fazer isso de outro jeito. Mas por enquanto só precisamos de duas dessas: **Left** e **Right**, então essa função irá servir.

Por fim, temos a função `update()` que é chamada depois da função `create()`, a função `update()` é chamada dentro do loop do jogo, então ela é chamada a cada 1/60s, é preciso de atenção aqui, todo carregamento pesado deve ser evitado aqui, pois poderá travar nosso jogo, temos que deixar essa função o mais "leve" possivel. Por exemplo, não seria legal colocarmos o código que está na função `preload()` aqui, pois só precisamos carregar a imagem do personagem uma vez, e seria processamento desnecessário carregar a imagem a cada 1/60s. Na função `update()` abaixo temos as condicionais para movimentar o personagem:

```javascript
  update() {
    if(this.cursors.left.isDown) {
      this.player.setVelocityX(-160)
    }else if(this.cursors.right.isDown) {
      this.player.setVelocityX(160)
    }else{
      this.player.setVelocityX(0)
    }
  }
```
Na primeira condição estamos verificando se a tecla "Left" está sendo pressionada com o booleano `isDown`, se estiver então configuramos a velocidade x do player, ou seja horizontal, para -180, isso fará ele se mover negativamente em x, positivo vai para direita e negativo para esquerda. Depois configuramos com `else if` outra condição, onde verificamos se a tecla "Right" foi pressionada, da mesma maneira, verificando o booleano `isDown`, se foi pressionada, então configuramos a velocidade x do player para 160, o oposto, agora ele andará para direita ao pressionar a tecla "Right". Por último quando nenhuma tecla for pressionada configuramos a velocidade x do player para 0, assim não ocorre de pressionamos para direita e depois soltarmos a tecla, e o personagem continuar se movendo para a direita infinitamente. Configurando o `else` ele irá ficar parado se nenhuma tecla for pressionada, que é o que queremos.

Se atente que quando o loop iniciar irá ser verificado se a tecla para esquerda foi pressionada, se ela foi, não será verificado as condições seguintes, e será executado o código configurando a velocidade x do player para -160, até a tecla ser solta, mesmo se você apertar a tecla para direita, se você apertou a tecla esquerda primeiro, irá apenas executar o código referente a tecla da esquerda, isso porque utilizamos `else if` ao invés de dois `if`.

Com isso temos nosso personagem se movendo na tela do jogo ao pressionarmos a seta para direita/esquerda.

Veja o personagem na tela com as ferramentas de desenvolvedor aberta (Use F12 para abrir as ferramentas de desenvolver no Chromium):

![Player and Canvas - Phaser 3](../images/condicionais-operadores-img7.png)

Se você foi atento, deve ter percebido que não usei `;` no final das instruções nos códigos Javascript, isso porque os compiladores/interpretadores de Javascript atualmente conseguem entender seu código sem você precisar terminar toda instrução com `;`.
Sinta-se livre para usar de uma meneira ou de outra, eu prefiro manter o código mais limpo sem os `;`.

## O que aprendi hoje?

* O que é uma condicional

* Como é utilizada as condicionais no português

* O que é `if`, `else`, `else if` no Javascript

* Quais são os operadores de igualdade/desigualdadde do Javascript

* Quais são os operadores relacionais do Javascript

* Quais são os operadores lógicos do Javascript

* Como utilizar condicionais e operadores juntos

* Como utilizar os operadores lógico NOT `!`, AND `&&` e OR `||`

* Como configurar uma árvore de diretório inicial para um jogo

* Como carregar a imagem do personagem na tela de jogo

* Como mover o sprite (imagem do personagem) para direita/esquerda

## Exercícios para casa

1. Traduza as seguites condições para código em Javascript:
  * SE eu estou vivo ENTÃO estou menos vivo a cada segundo.
  * SE o cachorro estiver andando ENTÃO o cachorro está feliz.
  * SE eu pressionar a tecla para direta ENTÃO meu personagem anda para direita.

2. Você quer fazer o seu personagem pular apenas quando está com frio, traduza isso para código Javascript.
  * Dica: você pode criar uma váriavel booleana chamada `var isCold = false`. "isCold" em portuguẽs é: "está com frio?".
