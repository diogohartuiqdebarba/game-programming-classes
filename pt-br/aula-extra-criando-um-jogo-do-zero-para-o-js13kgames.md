# Aula Extra - Criando um jogo do zero para o JS13KGAMES

## O que preciso para estudar essa aula?

## Introdução

Primeiro se ainda não conhece, irei apresentá-los a game jam js13kGames, a ideia é simples, uma competição onde
densevolvedores de jogos enviam seus jogos, porém há algumas regras, o seu jogo precisa ter no máximo 13kb zipado e
você precisa seguir o tema da competição, que nesse ano de 2019 é "back", traduzido para o português seria "voltar".
POderemos ver como é a criação de um jogo do início ao fim, para isso não usarei o Phaser, pois só o código do Phaser já
ultrapassaria os 13kb limite, então irei usar uma Game Engine minimalista chamada [Kontra](https://github.com/straker/kontra),
ela foi feita especificamente para a game jam js13kGames.

Antes de escrever qualquer código é necessário buscar idéias em nossa cabeça, e trazer elas para o papel ou tela, eu prefiro
fazer rascunhos rapidos no papel, faço um quadrado como se fosse a tela, e depois penso o que poderia ter dentro dessa tela
que poderia me divertir? e se vai surgindo ideias grandes demais, começo a ir cortando, vou criando outras telas pois a primeira
já ficou toda rasurada com meus cortes, até chegar em um jogo minimalista em que acredito poder caber em 13kb.

1. Fazer o personage andar direita/esquerda
2. Integrar a animação de movimento do personagem andando
3. Fazer personagem mover a privada
4. Fazer o personagem cagar na privada
5. Integrar animação da merda descendo pela privada
6. Integrar audio da descarga da privada
7. Fazer merda cair do céu aleatória
8. Fazer comida cair do céu aleatória
9. Fazer life ser reduzido se personagem não comer
10. Game over se merda encostar no chão ou life do personagem chegar a 0
11. Programar necessidade do personagem de cagar a cada 2 pães
12. Fazer mais merdas cairem do céu a cada cagada
13. Programar score de merdas coletadas com a privada
14. Fazer cena de game over com uma merda grande
15. Fazer cena de start game
16. Fazer música de fundo
17. Integrar audio de merda caindo na água
18. Integrar audio de mastigação de pão
19. Integrar audio de passos
20. Integrar audio de privada sendo empurrada
21. Integrar audio de merda caindo no chão


### Topico 1

## Estudando Jogando

## Exercícios para agora

## Programando

## O que aprendi hoje?

## Exercícios para casa