* [Introdução](pt-br/README.md)
* [Aula 1 - Algoritimos e Abstrações](pt-br/aula1-algoritmos-e-abstracoes.md)
* [Aula 2 - Variáveis e Constantes](pt-br/aula2-variaveis-e-constantes.md)
* [Aula 3 - Condicionais e Operadores](pt-br/aula3-condicionais-operadores.md)
* [Resolução de exercícios](pt-br/respostas-de-exercicios.md)