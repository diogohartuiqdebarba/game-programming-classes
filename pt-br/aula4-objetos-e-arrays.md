# Aula 4 - Objetos e Arrays - (Game Ghosts: Animação do personagem)

## O que preciso para estudar essa aula?

Para iniciar nesta primeira aula, você precisa ter estudado as 3 aulas anteriores, ou caso já tenha certo conhecimento de programação, para que consiga acompanhar o desenvolvimento do jogo Ghosts desta aula, você precisa ter um Sprite movendo em sua tela para direita e esquerda ao pressionar as setas para direita e esquerda do teclado, que é o que fazemos na aula 3.

## Introdução

Lembra que falamos dos tipos primitivos do Javascript na aula 2? Agora você veremos dois dos tipos não primitivos do Javascript, que são os Objetos e os Arrays.

## Estudando Jogando

[filename](../games/poison-kills-class4/index.html ':include :type=iframe width=100% height=480px scrolling=no')

## Exercícios para agora

1. O que são objetos no Javascript?

2. O que são arrays no Javascript?

## Programando



## O que aprendi hoje?

* O que são objetos no Javascript

* O que são arrays no Javascript

* 

## Exercícios para casa

1. 
  
2. 
