# Aula 1 - Algoritmos e Abstrações

## O que preciso para estudar essa aula?

Para iniciar nesta primeira aula, você precisa ter respondidas, 
mesmo que superficialmente, as perguntas abaixo:

* O que é um navegador Web?

* O que é uma página Web?

* O que é a Web?

* O que é um Web Server?

* O que é uma linguagem de programação?

Você precisa ter um conhecimento básico de informática, saber digitar, clicar, ver, 
pesquisar (isso você também irá aprender a aprimorar no decorrer das aulas). 
Também precisa ter acesso a um navegador, de preferência o [Chromium](https://www.chromium.org), pois será o navegador
que irei utilizar, e também é necessário uma extensão [Web Server](https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb) que será o Web Server que utilizarei, mas sinta-se a vontade para 
usar o navegador e o Web Server que mais lhe agradar.

Apenas isso, e claro, vontade de aprender.

## Introdução

Em nossa vida fazemos tarefas todos os dias, e muitas dessas tarefas exigem mais de um passo para
que sejam completadas, pense em uma tarefa do seu dia-a-dia, irei dar o exemplo que aqui pensei, 
eu tenho o costume de beber um copo d'água logo depois de acordar, então imagine que estou ainda em
minha cama, quais seriam os passos para eu completar minha tarefa de beber um copo d'água?

1. Levantar da cama
2. Ir até a cozinha
3. Pegar um copo
4. Encher o copo com água
5. Beber a água do copo

Perceba os passos acima para completar a tarefa de beber um copo d'água, com esses
passos eu conseguiria beber meu copo d'água, esses conjuntos de passos para resolver
uma tarefa podemos chamar de algoritmos. Então algoritmos são um conjunto de passos que
servem para resolver um problema. Entretanto, eu poderia também ser mais detalhado:

1. Sentar na cama
2. Colocar o pé direito no chão
3. Colocar o pé esquerdo no chão
4. Ficar em pé
5. Caminhar 5 passos
6. Abrir a porta do quarto
7. Andar 6 passos até a cozinha
8. Andar 4 passos até o guarda-copos
9. Pegar o copo d'água
10. Andar um passo até o filtro
11. Posicionar o copo vazio embaixo da torneira
12. Abrir a torneira
13. Esperar o copo se encher com a água
14. Fechar a torneira
15. Levar o copo até a boca
16. Beber a água do copo

Perceba agora que de 5 passos, precisei de 16 passos para completar a mesma tarefa, isso porque fui mais detalhado,
isso é um conceito muito comum na programação, e se chama "abstração". 

No modo mais abstraído utilizei apenas 5 passos, onde escondi certos "sub-passos", como por exemplo, 
o passo "1 - Levantar da cama", eu escondi que poderia ser necesário primeiro sentar na cama, 
depois colocar um pé no chão, depois o outro, depois ficar em pé. Os passos de 1 a 4 do modo de 16 passos
ainda são abstrações de outros passos, como por exemplo, em um nível de detalhes mais alto: respirar, abrir os olhos...

Então para fixar, veja abaixo cada uma das abstrações dos 5 passos com seus respectivos passos dos algoritmo mais detalhado:

**1. Levantar da cama** é uma abstração dos seguintes passos:
    1. Sentar na cama
    2. Colocar o pé direito no chão
    3. Colocar o pé esquerdo no chão
    4. Ficar em pé
    
**2. Ir até a cozinha** é uma abstração dos seguintes passos:
    5. Caminhar 5 passos
    6. Abrir a porta do quarto
    7. Andar 6 passos até a cozinha
    
**3. Pegar um copo** é uma abstração dos seguintes passos:
    8. Andar 4 passos até o guarda-copos
    9. Pegar o copo d'água
    
**4. Encher o copo com água** é uma abstração dos seguintes passos:
    10. Andar um passo até o filtro
    11. Posicionar o copo vazio embaixo da torneira
    12. Abrir a torneira
    13. Esperar o copo se encher com a água
    14. Fechar a torneira
    
**5. Beber a água do copo** é uma abstração dos seguintes passos:
    15. Levar o copo até a boca
    16. Beber a água do copo

Utilizamos os algortimos para resolver problemas, e as abstrações para deixar os algoritmos mais simples e também para 
reaproveitarmos as resoluções de problemas que já criamos. Na programação construíremos abstrações com algoritmos, 
isso pode parecer um pouco confuso de início, mas você irá conseguir entender com o tempo, só é preciso dizer que 
nem todas abstrações que você for utilizar será você que as escreveu, como aqui em nossas aulas estaremos utilizando 
as abstrações que o Motor de Jogo Phaser nos fornece que serão úteis para podermos criar os jogos sem precisar aprender 
tudo "do zero", como a física do jogo, o carregamento de ativos, os sons do jogo, entre outras coisas que o Phaser irá 
abstrair para a gente. Sendo assim, iremos utilizar algoritmos que não fomos nós que escrevemos para escrever nossos
próprios algoritmos.

Você irá utilizar muito algoritmos, então se tiver qualquer dúvida, essa é a hora de procurar entender o que ainda está em
dúvida, você tem o computador na sua frente e é livre para pesquisar, então abuse das ferramentas de pesquisas, e tire
suas dúvidas agora.

## Estudando Jogando

[filename](../games/doors-class1/index.html ':include :type=iframe width=100% height=480px scrolling=no')

## Exercícios para agora

1. O que é um algoritmo?

2. Dê um exemplo de um algoritmo.

3. O que é "abstração" na programação?

4. Dê um exemplo de abstração.

5. Crie um problema e crie um algoritmo para resolvê-lo (algoritmo com mínimo de 10 passos).

6. Crie abstrações para simplificar o algoritmo do exercício anterior.

## Programando

Hoje iremos mexer no código do jogo "Doors" do tópico "Estudando Jogando".

Primeiro você precisa baixar os arquivos do jogo, no link abaixo você pode acessar o repositório do jogo:

https://gitlab.com/diogohartuiqdebarba/game-programming-classes

Para baixar você pode usar o [git](https://git-scm.com/book/pt-br/v1/Primeiros-passos) ou então baixar o arquivo
no formato zip diretamente no link abaixo:

https://gitlab.com/diogohartuiqdebarba/game-programming-classes/-/archive/master/game-programming-classes-master.zip

Tendo baixado o arquivo, você deve descompactá-lo, e agora você tem um diretório chamado
"game-programming-classes", dentro dele você tem a pasta "games" que é onde se encontram os jogos da aula,
procure pelo diretório "doors" que é o que queremos, entre nele, agora você tem outros dois diretórios
e mais um arquivo chamado "index.html", esse arquivo "index.html" é o responsável por carregar os códigos de nosos jogo, 
para então o jogo ser iniciado e podermos vê-lo em nossa tela, vamos então abri-lo no navegador, você pode fazer 
isso de duas maneiras, clicando do lado direito do mouse e pedindo para abrir com o seu navegador ou então com 
o navegador já aberto digitar na barra de endereço:

```
file:///home/downloads/game-programming-classes/games/doors-class1/index.html
```

Isso irá abrir o arquivo "index.html" que está dentro do diretório "doors-class1". O diretório "doors-class1" está dentro do
diretório "games". O diretório "games" está dentro do diretório "game-programming-classes". O diretório 
"game-programming-classes" no meu caso está dentro do diretório "downloads". Meu diretório "downloads" está
dentro de meu diretório "home". Você precisa saber o caminho para chegar até seu arquivo "index.html", no seu
caso poderá ser um caminho diferente, fique atento onde descompactou o arquivo .zip.

Abrindo o arquivo irá perceber que algo deu errado, aparece uma tela com um quadrado com borda verde como abaixo:

![Game error - need web server!](../images/algoritmos-e-abstracoes-img1.png)

O que está acontecendo, se você apertar a tecla "F12" do seu teclado, irá abrir as ferramentas de
desenvolvedor do navegador, qual iremos usar bastante. Na aba console que é a aba que provavelmente
estará já aberta para você, vemos vários erros semelhantes ao erro abaixo:

```
Access to XMLHttpRequest at 
'file:///home/downloads/game-programming-classes/games/doors-class1/assets/doors-loading2.png'
from origin 'null' has been blocked by CORS policy: Cross origin requests 
are only supported for protocol schemes: 
http, data, chrome, chrome-extension, https.
```
O navegador não está permitindo que acessemos os nossos assets (no caso do erro acima, a imagem "doors-loading2.png", 
que é um dos assets do  jogo "Doors"), pois eles estão no sistema de arquivos de nosso computador, e por questões de 
segurança o navegador não permite o acesso direto de scripts ao sistema de arquivos, e isso é bom, não gostariamos de 
entrar em um site e ele poder acessar nossos arquivos pessoais, certo?

Para resolver essa questão precisamos utilizar um Web Service, que irá ser um servidor http, o que é isso?
um serviço que irá servir páginas html, os sites que acessamos na internet estão hospedados em um servidor
http que nos servem os arquivos do site (paginas HTML que podem ter vídeos, images, etc). Se você estiver usando o 
navegador Chromium ou Chrome, isso será muito fácil, só precisa instalar a extensão [Web Server](https://chrome.google.com/webstore/detail/web-server-for-chrome/ofhbbkphhbklhfoeikjpcbhemlocgigb) e executá-la,
após executada uma tela como mostrada abaixo será aberta:

![Web Server extension screen.](../images/algoritmos-e-abstracoes-img2.png)

Em "CHOOSE FOLDER" você irá procurar o diretório "games" do arquivo .zip descompactado, deixaremos servindo os
arquivos do diretório "games" todo e não só "doors-class1", pois nas próximas aulas haverá outros jogos. 

Após ter configurado o diretório "games", embaixo em "Web Server URL(s)" temos o endereço "http://127.0.0.1:8887" que
é nosso Web Service local, copie e cole esse endereço para a barra de endereço de seu navegador, você deverá
ter uma tela bem parecida com a tela abaixo:

![Games directory.](../images/algoritmos-e-abstracoes-img3.png)

Clique para entrar no diretório "doors-class1", se você deixou marcado nas configurações do Web Server "Automatically
show index.html" você irá cair direto na tela de jogo, pois está sendo aberto o arquivo "index.html" automaticamente
para você, caso contrário você deverá clicar no arquivo "index.html" para abrir o jogo.

Verá que você caiu no jogo como no tópico "Estudando Jogando". Mas agora não iremos jogar, você precisa abrir o
arquivo "index.html" em algum editor de código de sua preferência. Eu recomendo o [Brackets](http://brackets.io/), é o 
editor que estou utilizando. Você agora tem aberto um arquivo HTML que talvez você não compreenda muito o que
está escrito nele, mas não se preocupe, só vamos mudar uma linha nele para iniciarmos:

![Index file STUDY_MODE off.](../images/algoritmos-e-abstracoes-img4.png)

Mude o valor atribuido a constante "STUDY_MODE" de "false" (falso) para "true" (verdadeiro),
como fiz abaixo:

![Index file STUDY_MODE on.](../images/algoritmos-e-abstracoes-img5.png)

Agora atualize a página com pressionando a tecla "F5" de seu teclado, e veja o jogo sendo aberto no
modo de estudo:

![Index file STUDY_MODE on.](../images/algoritmos-e-abstracoes-img6.png)

Sim a tela diminuiu, e agora temos uns retângulos envolta do personagem, da chave e da porta, e ainda as paredes
ficaram todas de cor laranja. Esse é o modo debug da física de nosso jogo, tudo que está em laranja são colisores,
ou seja, podem colidir com nosso personagem, assim evitamos que o personagem saia da tela ou atravesse paredes,
isso tudo você irá aprender nas próximas aulas, estou apenas introduzindo você ao ambiente de um desenvolvedor de
jogos que utiliza o Motor de Jogo Phaser.

Então agora que você está com o jogo de estudo aberto, você precisa abrir no seu editor de código o arquivo
chamado "study-move-player.js" que está dentro do diretório "/game-programming-classes/games/doors-class1/js/", 
nele você tem o seguinte conteúdo:

```javascript
/* Code of Student */

function studyMovePlayer() {
  movePlayerLeftRightUpDown();
}

function movePlayerLeftRightUpDown() {
  const moves = [
    {
      direction: 'left',
      seconds: 1
    },
    {
      direction: 'right',
      seconds: 1
    },
    {
      direction: 'up',
      seconds: 1
    },
    {
      direction: 'down',
      seconds: 1
    }
  ];
  movePlayer(moves);
}

/* Code of Teacher */

async function movePlayer(moves) {
  const studyScene = game.scene.scenes[1];
  for(let i = 0; i < moves.length; i++) {
    studyScene.player.move(moves[i].direction);
    await sleep(moves[i].seconds * 1000);
    studyScene.player.move(); // stop move player
    await sleep(1000);
  }
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
```
Repare na primeira linha `/* Code of Student */`, isso é uma das maneiras de fazer comentários
dentro do código, há uma outra maneira qual também utilizei no código acima: `// stop move player`.
Para comentar várias linhas usamos:

```javascript
/* Meu comentário 
de várias 
linhas
*/
```

e para comentar apenas uma linha:

```javascript
const ZERO = 0; // Number zero
``` 
Veja acima que só fica comentado o texto após as barras, e somente naquela linha. Não é necessário
você comentar tudo em seu código, a melhor maneira de lidar com o código é escrevê-lo de maneira
que não precisamos comentar nada para explicar ele, devemos sempre tentar escrever códigos que se
"auto-explicam", dando bons nomes para nossas funções, váriaveis, objetos, etc. Mas ainda não lhe disse o
que é uma função ou uma váriavel, ou um objeto, e nessa aula ainda não nos aprofundaremos nisso, estamos
estudando algoritimos e abstrações.

Você não precisa se preocupar com o código do professor (tudo que está abaixo de `/*Code of Teacher*/`), eu estarei criando abstrações para você 
utilizar por enquanto, mas você precisa se atentar ao código do estudante (Code of Student):

```javascript
/* Code of Student */

function studyMovePlayer() {
  movePlayerLeftRightUpDown();
}

function movePlayerLeftRightUpDown() {
  const moves = [
    {
      direction: 'left',
      seconds: 1
    },
    {
      direction: 'right',
      seconds: 1
    },
    {
      direction: 'up',
      seconds: 1
    },
    {
      direction: 'down',
      seconds: 1
    }
  ];
  movePlayer(moves);
}
```
Acima temos uma função chamada "studyMovePlayer", uma função é semelhante a um algoritmo, mas uma função 
não é um algoritmo, pois ela é mais que um algoritmo, ela utiliza um algoritmo em seu interior, 
ela pode receber valores para manipular, e pode também retornar valores para serem manipulados
por outras funções. 

A função "studyMovePlayer" será chamada no inicio do nosso jogo, qual não
irei mostrar o código dessa parte agora, pois seria necessário ensinar alguns conceitos do Phaser antes
de apresentar essa parte do código para vocês, e já estamos cheios de conceitos novos aqui: funções,
váriaveis, constantes, objetos, etc, quais ainda nem sabemos o que é, não ligue para isso, nas próximas
aulas veremos cada um desses conceitos de maneira a ficar simples de entender.

Agora voltando ao assunto da aula, "Algoritmos e Abstrações". 

Dentro da função "studyMovePlayer" que é chamada no inicio do jogo, temos outra função de nome 
"movePlayerLeftRightUpDown", então o que acontece: o jogo começa e a função "studyMovePlayer" é chamada, 
e dentro dessa função é chamada a função "movePlayerLeftRightUpDown".

É como se chamassemos "movePlayerLeftRightUpDown" ao iniciar o jogo, com a vantagem que podemos acrescentar 
mais coisas na função "studyMovePlayer" para serem executadas, está conseguindo relacionar? os passos do 
algoritmo e as chamadas de funções? podemos dizer que a função "studyMovePlayer" tem um algoritmo de apenas um 
passo chamado "movePlayerLeftRightUpDown", então seu dever será adicionar mais passos nessa função seguindo o 
padrão da função "movePlayerLeftRightUpDown". 

**O Problema 1:** 

>Você deverá construir uma nova função para mover o jogador (Player) para **direita, direita, esquerda, esquerda**

Lembrando que suas traduções para o inglês, no caso do código, são:

```
direita = right
esquerda = left
trás/baixo = down
frente/cima = up
```

Você deverá escrever o nome da função em inglês também seguindo o padrão, vamos antes fazer um exemplo para mover
o jogador para: **direita, esquerda**. Você então deve criar uma função assim:

```javascript
/* Code of Student */

function studyMovePlayer() {
  movePlayerLeftRightUpDown();
  movePlayerRightLeft();
}

function movePlayerRightLeft() {
  const moves = [
    {
      direction: 'right',
      seconds: 1
    },
    {
      direction: 'left',
      seconds: 1
    }
  ];
  movePlayer(moves);
}
```
Atualize a página pressionando "F5" e veja o personagem andar ainda mais pela cena do jogo.

Percebeu como fica o nome da função, se é para mover para direita e depois para esquerda, então fica "move + Playe + Right + Left",
juntamos tudo deixando apenas as primeiras letras de cada palavra maiúscula, isso se chama [Camel Case](https://pt.wikipedia.org/wiki/CamelCase), então unindo tudo fica "movePlayerRightLeft", você deve fazer o mesmo para
o problema 1.

Não ligue para as partes do código que ainda não compreende, apenas tente seguir o padrão e fazer como no exemplo anterior.

Conseguiu? Pronto! você criou uma nova abstração para chamar a função que move o personagem na cena do jogo. Viu como
funcionam as abstrações, você ainda nem aprendeu a programar em JavaScript e já está conseguindo fazer o personagem
andar na cena do jogo, você não precisou saber o que o código faz para conseguir resolver o problema que queria, essa
é uma das utilidades das abstrações, entretanto também é importante saber o que o código faz, mas tudo no seu tempo, 
vamos para o próximo problema.

**O Problema 2:**

>Faça o personagem andar para a direita por 3 segundos.

Calma aí! você ainda não ensinou como faço o personagem mover por uma quantidade de segundos que eu quero! Sim, é verdade,
e nem vou ensinar. Você terá que descobrir isso. Passe o olho no código novamente, e tente criar uma função, você pode chama-lá 
assim "movePlayerRight3seconds". Já dei uma dica.

Mas vou ampliar mais a dica, as traduções em inglês:

```
direction = direção
seconds = segundos
move = mover
player = jogador
```
Na programação de jogos é comum chamarmos o personagem de "player" ou "character". Character significa "personagem". Mas em 
ambos geralmente queremos dizer do personagem do jogo.

Conseguiu? Eu acredito que sim, mas caso não tenha, você poderá olhar as [respostas dos exercícios](pt-br/respostas-de-exercicios.md).
Contudo, antes tente, não vá direto nas respostas, não se engane, você tem que quebrar a cabeça, programação é isso,
tem que ir trabalhando o reconhecimento de padrões e habilidade em resolver problemas, com o tempo você vai resolvê-los
cada vez mais rápido, não tenha pressa, tome o tempo que precisar para resolver, e evite olhar as respostas.

Essa aula acaba aqui. Não se esqueça de no tópico abaixo "O que aprendi hoje?" você ter certeza que apredeu tudo que está nele,
se não, é uma boa ideia, rever toda a aula desde o início, ou no ponto que você acha que não conseguiu aprender. Não se esqueça
de fazer também os execícios para casa, é importante. Até a próxima aula.

## O que aprendi hoje?

* O que é um algoritmo?

* Para que serve um algoritmo?

* Como construir algoritmos?

* O que é uma abstração?

* Criar um algoritimo para resolver um problema.

* Criar uma abstração para simplificar um conjunto de passos de um algoritmo.

## Exercícios para casa

1. Crie algoritmos para resolver os problemas abaixo:

  * Você precisa ir até a escola.
  
  * Você quer pular de paraquedas.
  
  * Você precisa falar com seu amigo.
  
2. Crie abstrações para os passos dos algoritmos que você criou no exercício anterior.
